<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Admin extends Panel{
        function __construct() {
            parent::__construct();
        }
        
        function empresas($action = '',$id = ''){
            $crud = $this->crud_function('','');
            $crud->set_field_upload('portada','img/empresas');
            $crud->set_field_upload('portada_main','img/empresas');
            $crud->set_field_upload('banner','img/empresas');
            $crud->display_as('portada','Portada (800x565)');
            $crud->display_as('portada_main','Portada (480x350)');
            $crud->display_as('banner','Banner (1920x1280)');
            $crud->field_type('mapa','map',array());
            $crud->field_type('caracteristicas','tags');
            $crud->add_action('<i class="fa fa-envelope"></i> Comentarios','',base_url('productos/admin/empresas_comentarios').'/');
            $crud->add_action('<i class="fa fa-image"></i> Galeria de fotos','',base_url('productos/admin/empresas_fotos').'/');
            $crud = $crud->render();
            $this->loadView($crud);
        } 
        
        function categorias($action = '',$id = ''){
            $crud = $this->crud_function('','');
            $crud->set_subject('Categoria');
            $crud->add_action('<i class="fa fa-box"></i> Productos','',base_url('productos/admin/productos').'/');
            $crud = $crud->render();
            $crud->title = 'Categoria de productos';
            $this->loadView($crud);
        }
        
        function productos($categorias_id = '', $action = '',$id = ''){
            $crud = $this->crud_function('','');
            $crud->where('categorias_id',$categorias_id);
            $crud->field_type('categorias_id','hidden',$categorias_id);
            $crud->set_field_upload('portada','img/productos');
            $crud->set_field_upload('portada_main','img/productos');
            $crud->set_field_upload('banner','img/productos');
            $crud->display_as('portada','Portada (800x565)');
            $crud->display_as('portada_main','Portada (480x350)');
            $crud->display_as('banner','Banner (1920x1280)');
            $crud->field_type('mapa','map',array());
            $crud->add_action('<i class="fa fa-envelope"></i> Comentarios','',base_url('productos/admin/productos_comentarios').'/');
            $crud->add_action('<i class="fa fa-image"></i> Galeria de fotos','',base_url('productos/admin/productos_fotos').'/');
            $crud = $crud->render();
            $this->loadView($crud);
        } 
        
        function reservas_productos($action = '',$id = ''){
            $crud = $this->crud_function('','');
            $crud->where('productos_id',$action);
            $crud->field_type('productos_id','hidden',$action);            
            $crud = $crud->render();
            $this->loadView($crud);
        }
        
        function paquetes($action = '',$id = ''){
            $crud = $this->crud_function('','');
            $crud->set_field_upload('portada','img/productos');
            $crud->set_field_upload('portada_main','img/productos');
            $crud->set_field_upload('banner','img/productos');
            $crud->display_as('portada','Portada (800x565)');
            $crud->display_as('portada_main','Portada (480x350)');
            $crud->display_as('banner','Banner (1920x1280)');
            $crud->field_type('mapa','map',array());
            $crud->field_type('actividades','tags');
            $crud->add_action('<i class="fa fa-envelope"></i> Comentarios','',base_url('productos/admin/productos_comentarios').'/');
            $crud->add_action('<i class="fa fa-image"></i> Galeria de fotos','',base_url('productos/admin/productos_fotos').'/');
            $crud = $crud->render();
            $this->loadView($crud);
        }     
        
        function productos_comentarios($action = '',$id = ''){
            $crud = $this->crud_function('','');
            $crud->where('productos_id',$action);
            $crud->field_type('productos_id','hidden',$action);            
            $crud = $crud->render();
            $this->loadView($crud);
        }
        
        function empresas_comentarios($action = '',$id = ''){
            $crud = $this->crud_function('','');
            $crud->where('empresas_id',$action);
            $crud->field_type('empresas_id','hidden',$action);            
            $crud = $crud->render();
            $this->loadView($crud);
        }
        
        function empresas_fotos(){
            $this->load->library('image_crud');
            $crud = new image_CRUD();
            $crud->set_table('empresas_fotos')
                 ->set_url_field('foto')
                 ->set_relation_field('empresas_id')
                 ->set_image_path('img/empresas')
                 ->module = 'productos';
            $crud = $crud->render();
            $crud->output.= '<p><b>Nota: </b> El tamaño recomendado para las fotos es de 1170x480</p>';
            $crud->title = 'Galería Fotográfica de empresas';
            $this->loadView($crud);
        }
        
        function productos_fotos(){
            $this->load->library('image_crud');
            $crud = new image_CRUD();
            $crud->set_table('productos_fotos')
                 ->set_url_field('foto')
                 ->set_relation_field('productos_id')
                 ->set_image_path('img/productos')
                 ->module = 'productos';
            $crud = $crud->render();
            $crud->output.= '<p><b>Nota: </b> El tamaño recomendado para las fotos es de 1170x480</p>';
            $crud->title = 'Galería Fotográfica de productos';
            $this->loadView($crud);
        }
    }
?>
