<?php 
    require_once APPPATH.'/controllers/Main.php';    
    class Frontend extends Main{
        function __construct() {
            parent::__construct();
        }
        
        function empresas(){
            $empresas = new Bdsource();
            $empresas->limit = array('6','0');            
            if(!empty($_GET['direccion'])){
                $empresas->like('nombre',$_GET['direccion']);
            }
            //$blog->where('idioma',$_SESSION['lang']);
            if(!empty($_GET['page'])){
                $empresas->limit = array(($_GET['page']-1),6);
            }
            $empresas->init('empresas');
            foreach($this->empresas->result() as $n=>$b){
                $this->empresas->row($n)->link = site_url('empresa/'.toURL($b->id.'-'.$b->nombre));
                $this->empresas->row($n)->portada = base_url('img/empresas/'.$b->portada);
            }
            $totalpages = round($this->db->get_where('empresas')->num_rows()/6);
            $totalpages = $totalpages==0?'1':$totalpages;
            $this->loadView(
                array(
                    'view'=>'empresas',
                    'detail'=>$this->empresas,
                    'total_pages'=>$totalpages,
                    'title'=>'Empresas',
            ));
        }
        
        function productos($id = ''){
            $id = explode('-',$id);
            $id = $id[0];
            if(is_numeric($id)){
                $productos = new Bdsource();
                $productos->select = 'empresas.*, empresas.nombre as nombre';
                $productos->where('id',$id);                
                $productos->init('empresas',TRUE);
                $comentarios = new Bdsource();
                $comentarios->where('empresas_id',$this->empresas->id);
                $comentarios->init('empresas_comentarios');
                
                $recomendados = new Bdsource();
                $recomendados->limit = array(3,0);
                $recomendados->select = 'empresas.*, empresas.nombre as nombre, empresas.nombre as empresa';
                $recomendados->where('empresas.id !=',$this->empresas->id);
                $recomendados->init('empresas',FALSE,'recomendados');
                foreach($this->recomendados->result() as $n=>$b){
                    $this->recomendados->row($n)->link = site_url('empresa/'.toURL($b->id.'-'.$b->nombre));
                    $this->recomendados->row($n)->foto = base_url('img/empresas/'.$b->portada);
                }
                
                $fotos = new Bdsource();
                $fotos->where('empresas_id',$this->empresas->id);
                $fotos->init('empresas_fotos');
                $this->loadView(
                    array(
                        'view'=>'empresa',
                        'detail'=>$this->empresas,
                        'title'=>$this->empresas->nombre,
                        'fotos'=>$this->empresas_fotos,
                        'recomendados'=>$this->recomendados,
                        'comentarios'=>$this->empresas_comentarios
                    ));
            }else{
                $empresas = new Bdsource();
                $empresas->limit = array('6','0');            
                if(!empty($_GET['direccion'])){
                    $empresas->like('nombre',$_GET['direccion']);
                }
                //$blog->where('idioma',$_SESSION['lang']);
                if(!empty($_GET['page'])){
                    $empresas->limit = array(($_GET['page']-1),6);
                }
                $empresas->init('empresas');
                foreach($this->empresas->result() as $n=>$b){
                    $this->empresas->row($n)->link = site_url('empresa/'.toURL($b->id.'-'.$b->nombre));
                    $this->empresas->row($n)->portada = base_url('img/empresas/'.$b->portada);
                }
                $totalpages = round($this->db->get_where('empresas')->num_rows()/6);
                $totalpages = $totalpages==0?'1':$totalpages;
                $this->loadView(
                    array(
                        'view'=>'empresas',
                        'detail'=>$this->empresas,
                        'total_pages'=>$totalpages,
                        'title'=>'Grupos',
                ));
            }
        }
        
        function categorias($id = ''){
            $id = explode('-',$id);
            $id = $id[0];
            if(is_numeric($id)){
                $productos = new Bdsource();
                $productos->select = 'productos.*, productos.productos_nombre as nombre';
                $productos->where('categorias_id',$id);                
                $productos->init('productos',FALSE,'productos');
                foreach($this->productos->result() as $n=>$b){
                    $this->productos->row($n)->link = site_url('producto/'.toURL($b->id.'-'.$b->nombre));
                    $this->productos->row($n)->foto = base_url('img/productos/'.$b->portada);
                }                                
                $this->loadView(
                    array(
                        'view'=>'empresas',
                        'detail'=>$this->productos,
                        'title'=>$this->productos->row()->nombre,                 
                    ));
            }
        }
        
        function read($id){
            $id = explode('-',$id);
            $id = $id[0];
            if(is_numeric($id)){
                $productos = new Bdsource();
                $productos->select = 'productos.*, productos.productos_nombre as nombre';
                $productos->where('id',$id);                
                $productos->init('productos',TRUE);
                $comentarios = new Bdsource();
                $comentarios->where('productos_id',$this->productos->id);
                $comentarios->init('productos_comentarios');
                
                $recomendados = new Bdsource();
                $recomendados->limit = array(3,0);
                $recomendados->select = 'productos.*, productos.productos_nombre as nombre, empresas.nombre as empresa';
                $recomendados->where('productos.empresas_id',$this->productos->empresas_id);                
                $recomendados->where('productos.id !=',$this->productos->id);                
                $recomendados->join = array('empresas');
                $recomendados->init('productos',FALSE,'recomendados');
                foreach($this->recomendados->result() as $n=>$b){
                    $this->recomendados->row($n)->link = site_url('producto/'.toURL($b->id.'-'.$b->productos_nombre));
                    $this->recomendados->row($n)->foto = base_url('img/productos/'.$b->portada);
                }
                
                $fotos = new Bdsource();
                $fotos->where('productos_id',$this->productos->id);
                $fotos->init('productos_fotos');
                $this->loadView(
                    array(
                        'view'=>'producto',
                        'detail'=>$this->productos,
                        'title'=>$this->productos->nombre,
                        'fotos'=>$this->productos_fotos,
                        'recomendados'=>$this->recomendados,
                        'comentarios'=>$this->productos_comentarios
                    ));
            }else{
                throw new Exception('No se encuentra la entrada solicitada',404);
            }
        }
        
        function reservar(){
            $this->load->library('form_validation');
            $this->form_validation->set_rules('nombre','Nombre','required')
                                  ->set_rules('apellido','Apellido','required')
                                  ->set_rules('instituto','Instituto','required')
                                  ->set_rules('email','Email','required|valid_email|is_unique[reservas.email]')
                                  ->set_rules('direccion','Dirección','required')
                                  ->set_rules('ciudad','Ciudad','required')
                                  ->set_rules('cp','CP','required')
                                  ->set_rules('provincia','Provincia','required')
                                  ->set_rules('telefono','Telefono','required')
                                  ->set_rules('productos_id','Paquete','required');
            if($this->form_validation->run()){
                $this->db->insert('reservas_productos',$_POST);
                echo $this->success('Su reserva ha sido cargada con éxito <script>document.location.reload()</script>');
                $this->enviarcorreo((object)$_POST,2,'info@kanvoy.com');
                $this->enviarcorreo((object)$_POST,3);
            }else{
                echo $this->error('Por favor complete todos los parametros solicitados <script>$("#place_order").attr("disabled",false); </script>');
            }
        }
        
        function comentario_empresa($action = '',$id = ''){
            $this->load->library('form_validation');
            $this->form_validation->set_rules('nombre','Nombre','required')
                                  ->set_rules('apellido','Apellido','required')
                                  ->set_rules('telefono','Telefono','required')
                                  ->set_rules('titulo','Titulo','required')
                                  ->set_rules('mensaje','Mensaje','required')
                                  ->set_rules('empresas_id','Destino','required');
            if($this->form_validation->run()){
                $_POST['fecha'] = date("Y-m-d");
                $this->db->insert('empresas_comentarios',$_POST);
                echo $this->success('Su comentario ha sido cargado con éxito <script>document.location.reload()</script>');
            }else{
                echo $this->error('Por favor complete todos los parametros solicitados <script>$("#guardar").attr("disabled",false); </script>');
            }
        }    
                
        function comentario($action = '',$id = ''){
            $this->load->library('form_validation');
            $this->form_validation->set_rules('nombre','Nombre','required')
                                  ->set_rules('apellido','Apellido','required')
                                  ->set_rules('telefono','Telefono','required')
                                  ->set_rules('titulo','Titulo','required')
                                  ->set_rules('mensaje','Mensaje','required')
                                  ->set_rules('productos_id','Destino','required');
            if($this->form_validation->run()){
                $_POST['fecha'] = date("Y-m-d");
                $this->db->insert('productos_comentarios',$_POST);
                echo $this->success('Su comentario ha sido cargado con éxito <script>document.location.reload()</script>');
            }else{
                echo $this->error('Por favor complete todos los parametros solicitados <script>$("#guardar").attr("disabled",false); </script>');
            }
        }                 
    }
?>
