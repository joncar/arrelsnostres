<header><?php $this->load->view('includes/template/header'); ?> 
    <!-- breadcrumbs start-->
    <section style="background-image: url('http://kanvoy.com/pic/breadcrumbs/bg-1.jpg'); background-repeat: no-repeat; background-position: center;" class="breadcrumbs" data-mce-style="background-image: url('http://kanvoy.com/pic/breadcrumbs/bg-1.jpg'); background-repeat: no-repeat; background-position: center;">
        <div class="container">
            <div class="text-left breadcrumbs-item">
                <a href="#">Inicio</a><i>/</i> 
                <a href="#" class="last"><span>Empresas</span></a>
                <h2><span>EMPR</span>ESAS</h2>
            </div>
        </div>
    </section>
    <!-- ! breadcrumbs end-->
</header>
<div class="content-body">
    <div class="container page">
        <h2 class="title-section mb-5">
            <span>LES NOSTRES</span> EMPRESES         
        </h2>
        <div class="row">
            <!-- Recomended item-->
            <?php foreach($detail->result() as $d): ?>
                <?php $this->load->view('_item2',array('d'=>$d)); ?>
            <?php endforeach ?>
            <?php if($detail->num_rows()==0): ?>
                <div class="col-md-12">
No s'han trobat resultats per als paràmetres de cerca, si us plau intenti de nou
                </div>
            <?php endif ?>
        </div>
    </div>
    <!-- call out section-->
    <?php $this->load->view('includes/template/subscribe'); ?>
    <!-- ! call out section	-->
</div>