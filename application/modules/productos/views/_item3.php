<div class="col-md-6 col-xs-12">
    <div class="shop-item">
        <!-- Shop Image-->
        <div class="shop-media">
            <div class="pic" align="center">
                <img src="<?= $d->portada ?>" data-at2x="<?= $d->portada ?>" alt="" style="width:339px; height:229px; margin-left:80px;">
            </div>
            <div class="location"><?= $d->nombre ?></div>
        </div>
        <!-- Shop Content-->
        <div class="shop-item-body">
            <a href="#">
                <h6 class="shop-title"><?= $d->nombre ?></h6>
            </a>
            <p class="mb-30"><?= $d->descripcion ?></p>
        </div>
        <div class="link"> 
            <a href="<?= $d->portada ?>" class="fancy">
                <i class="fa fa-expand"></i>
            </a>
        </div>
    </div>
</div>