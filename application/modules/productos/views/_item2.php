<div class="col-md-6">
    <div class="recom-item border">
        <div class="recom-media">
            <a href="<?= $d->link ?>">
                <div class="pic">
                    <img src="<?= $d->portada ?>" data-at2x="<?= $d->portada ?>" alt="" style="width: 720px;">
                </div>
            </a>
            <div class="location">
                <?php 
                    $this->db->select('productos.*');
                    $this->db->join('categorias','categorias.id = productos.categorias_id'); 
                    echo $this->db->get_where('productos',array('empresas_id'=>$d->id))->num_rows() 
                ?> Productos
            </div>
        </div>
        <!-- Recomended Content-->
        <div class="recom-item-body">
            <a href="<?= $d->link ?>">
                <h6 class="blog-title"><?= $d->nombre ?></h6>
            </a>
            <p class="mb-30"><?= $d->descripcion_corta ?></p>
                <a href="<?= site_url() ?>p/contacto.html" class="recom-button">Contactar</a>  
                <a href="<?= $d->link ?>" class="cws-button small alt">Ver Productos</a> 
                          
        </div>
        <!-- Recomended Image-->
    </div>
</div>

