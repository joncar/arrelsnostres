<header>
    <?php $this->load->view('includes/template/header'); ?>
    <!-- breadcrumbs start-->
    <section style="background-image:url('<?= base_url('img/empresas/'.$detail->banner) ?>'); background-repeat: no-repeat; background-position: center;" class="breadcrumbs style-2 gray-90">
      <div class="container">
        <div class="text-left breadcrumbs-item">
            <a href="#">Inicio</a><i>/</i>
            <a href="#">Empreses</a><i>/</i>
            <a href="#" class="last"><?php echo $detail->nombre ?></a>
          <h2>
              <span>Empresa</span> <?php echo $detail->nombre ?>
          </h2>
          <div class="location"><i class="flaticon-suntour-map"></i>
            <p class="font-4"><?php echo $detail->direccion ?></p>
            <a href="#location" class="scrollto">
                Veure mapa
            </a>
          </div>
        </div>
      </div>
    </section>
</header>    
    <!-- ! breadcrumbs end-->
<!-- End Navigation panel-->           
    <div class="content-body">
      <section class="page-section pt-0 pb-50">
        <div class="container">
          <div class="menu-widget with-switch mt-30 mb-30">
            <ul class="magic-line">
                <li class="current_item"><a href="#overview" class="scrollto">Descripció</a></li>
                <li>
                    <a href="#" class="mn-has-sub">
                        Productes <i class="fa fa-angle-down button_open"></i>
                    </a>
                    <ul class="mn-sub" style="display: none; left:140px; z-index: 1000; height: 80px;">                        
                        <?php foreach($this->db->get_where('categorias',array('empresas_id'=>$detail->id))->result() as $p): ?>
                            <a href="<?= site_url('categorias/'.toURL($p->id.'-'.$p->categorias_nombre)) ?>">
                                <li>                                
                                        <?= $p->categorias_nombre ?>                                
                                </li>                        
                            </a>
                        <?php endforeach ?>
                    </ul>
                </li>
                <li><a href="#location" class="scrollto">Ubicació</a></li>
                <li><a href="#amenties" class="scrollto">Característiques</a></li>
                <li><a href="#reviews" class="scrollto">Comentaris</a></li>
                <li id="magic-line" style="width: 122px; left: 23.55px;"></li>
                <li id="magic-line"></li>
            </ul>
          </div>
        </div>
        <div class="container">
          <div id="flex-slider" class="flexslider">
            
          <div class="flex-viewport" style="overflow: hidden; position: relative;"></div>
          <ul class="flex-direction-nav">
              <li class="flex-nav-prev">
                  <a class="flex-prev flex-disabled" href="#" tabindex="-1">Anterior</a>
              </li>
              <li class="flex-nav-next">
                  <a class="flex-next" href="#">Següent</a>
              </li>
          </ul>
          <div class="flex-viewport" style="overflow: hidden; position: relative;">
              <ul class="slides" style="width: 2000%; transition-duration: 0s; transform: translate3d(0px, 0px, 0px);">
                  <?php foreach($fotos->result() as $f): ?>
                    <li class="flex-active-slide" style="width: 1170px; margin-right: 0px; float: left; display: block;">
                        <img src="<?= base_url('img/empresas/'.$f->foto) ?>" alt="" draggable="false">
                    </li>
                  <?php endforeach ?>
              </ul>
          </div>
          <ul class="flex-direction-nav">
              <li class="flex-nav-prev">
                  <a class="flex-prev flex-disabled" href="#" tabindex="-1">Anterior</a>
              </li>
              <li class="flex-nav-next">
                  <a class="flex-next" href="#">Següent</a>
              </li>
          </ul>
          </div>
          <div id="flex-carousel" class="flexslider">            
            <div class="flex-viewport" style="overflow: hidden; position: relative;"></div>
            <ul class="flex-direction-nav">
                <li class="flex-nav-prev">
                    <a class="flex-prev flex-disabled" href="#" tabindex="-1">Anterior</a>
                </li>
                <li class="flex-nav-next"><a class="flex-next" href="#">Següent</a></li>
            </ul>
            <div class="flex-viewport" style="overflow: hidden; position: relative;">
                <ul class="slides" style="width: 2000%; transition-duration: 0s; transform: translate3d(0px, 0px, 0px);">
                    <?php foreach($fotos->result() as $f): ?>
                        <li class="flex-active-slide" style="width: 162px; height:112px; margin-right: 5px; float: left; display: block;">                            
                            <img src='<?= base_url('img/empresas/'.$f->foto) ?>' style="height:112px; width:auto;">                                
                        </li>
                    <?php endforeach ?>
                </ul>
            </div>
            <ul class="flex-direction-nav">
                <li class="flex-nav-prev">
                    <a class="flex-prev flex-disabled" href="#" tabindex="-1">Anterior</a>
                </li>
                <li class="flex-nav-next">
                    <a class="flex-next" href="#">Següent</a>
                </li>
            </ul>
          </div>
        </div>
        <div class="container mt-30">
          <h4 class="mb-20"><?php echo $detail->descripcion_corta ?></h4>
          <div class="row">
            <div class="col-md-8">
              <?php echo $detail->descripcion ?>
            </div>
            <div class="col-md-4">
              <div class="bg-gray-3 p-30-40">
                    <ul class="style-1 mb-0">
                      <?php foreach(explode(',',$detail->caracteristicas) as $a): ?>
                        <li><?php echo $a ?></li>
                      <?php endforeach ?>
                    </ul>
                    <a href="#">
                        <ins class="alt-5">Més característiques</ins>
                    </a>
              </div>
            </div>
          </div>
        </div>
        <!-- section prices-->
        
        <!-- section location-->
        <div id="location" class="container mb-50" style="margin-top:20px;">
          <div class="row">
            <div class="col-md-8">
              <!-- section amenties-->
                <div id="amenties" class="mb-50">
                  <div class="row">
                    <div class="col-md-12">
                      <h4 class="trans-uppercase mb-10">Detalls de l'empresa</h4>
                      <div class="cws_divider mb-10"></div>
                    </div>
                  </div>
                  <div class="row mt-0 masonry" style="position: relative; height: 403.2px;">
                    <div class="col-md-12" style="position: absolute; left: 0px; top: 0px;">
                      <?= $detail->detalles ?>
                    </div>            
                  </div>
                </div>
              
              <!-- section amenties-->
                <div id="productos" class="mb-50">
                  <div class="row">
                    <div class="col-md-12">
                      <h4 class="trans-uppercase mb-10">Productes</h4>
                      <div class="cws_divider mb-30"></div>
                      <div class="col-md-12 products">
                        <?php 
                          $this->db->select('productos.*');
                          $this->db->join('categorias','categorias.id = productos.categorias_id');
                          foreach($this->db->get_where('productos',array('empresas_id'=>$detail->id))->result() as $d): 
                              $d->link = site_url('producto/'.toURL($d->id.'-'.$d->productos_nombre));
                              $d->portada = base_url('img/productos/'.$d->portada);
                              $d->nombre = $d->productos_nombre;
                              $this->load->view('_item3',array('d'=>$d)); 
                        ?>                        
                        <?php endforeach ?>
                      </div>
                    </div>
                  </div>
                </div>
              
            </div>
              <div class="col-md-4"  id="prices">
                  <h4 class="trans-uppercase mb-10">Ubicació</h4>
                    <div class="cws_divider mb-30"></div>
                    <!-- google map-->
                    <div class="map-wrapper" id="map"></div>
                    <ul class="icon inline mt-20">
                      <li> <a href="#"><?php echo $detail->direccion ?><i class="flaticon-suntour-map"></i></a></li>
                      <li> <a href="#"><?php echo $detail->telefono ?><i class="flaticon-suntour-phone"></i></a></li>
                    </ul>
                  <h4 class="trans-uppercase mb-10">CONTACTAR</h4>
                  <div class="cws_divider mb-30"></div>
                    <form name="checkout" method="post" action="shop-checkout.html" onsubmit="return reservar(this)" class="checkout woocommerce-checkout">
                      <div id="customer_details" class="col2-set">
                        <div class="mb-sm-50">


                          <div class="billing-wrapper">
                            <div class="woocommerce-billing-fields">

                              <p id="billing_first_name_field" class="form-row form-row-first validate-required">
                                <label for="billing_first_name">Nom<abbr title="required" class="required">*</abbr></label>
                                <input id="billing_first_name" name="nombre" placeholder="" value="" class="input-text" type="text">
                              </p>
                              <p id="billing_last_name_field" class="form-row form-row-last validate-required">
                                <label for="billing_last_name">Cognoms<abbr title="required" class="required">*</abbr></label>
                                <input id="billing_last_name" name="apellido" placeholder="" value="" class="input-text" type="text">
                              </p>
                              <div class="clear"></div>
                              <p id="billing_company_field" class="form-row form-row-wide">
                                <label for="billing_company">Empresa</label>
                                <input id="billing_company" name="instituto" placeholder="" value="" class="input-text" type="text">
                              </p>
                              <p id="billing_postcode_field" class="form-row form-row-wide">
                                <label for="billing_postcode">Email<abbr title="required" class="required">*</abbr></label>
                                <input id="billing_postcode" name="email" placeholder="Email" value="" class="input-text" type="text">
                              </p>
                              <p id="billing_address_1_field" class="form-row form-row-wide address-field validate-required">
                                <label for="billing_address_1">Adreça<abbr title="required" class="required">*</abbr></label>
                                <input id="billing_address_1" name="direccion" placeholder="Carrer/pis/ etc" value="" class="input-text" type="text">
                              </p>
                              <p id="billing_address_2_field" class="form-row form-row-wide address-field">
                                <input id="billing_address_2" name="ciudad" placeholder="Ciutat" value="" class="input-text" type="text">
                              </p>
                              <p id="billing_city_field" class="form-row form-row-wide address-field validate-required">
                                <input id="billing_city" name="provincia" placeholder="Provincia" value="" class="input-text" type="text">
                              </p>
                              <p id="billing_state_field" class="form-row form-row-first address-field validate-required validate-state">
                                <label for="billing_state">Códig Postal<abbr title="required" class="required">*</abbr></label>
                                <input id="billing_state" value="" placeholder="" name="cp" class="input-text" type="text">
                              </p>

                              <div class="clear"></div>
                              <p id="billing_email_field" class="form-row form-row-last">
                                <label for="billing_email">Telèfon<abbr title="required" class="required">*</abbr></label>
                                <input id="billing_email" name="telefono" placeholder="" value="" class="input-text" type="text">
                              </p>
                              <div class="clear"></div>
                            </div>
                            <div class="woocommerce-shipping-fields mt-10">                                                          
                                <label for="order_comments">Comentari</label>
                                <textarea id="order_comments" name="comentario" placeholder="" rows="2" cols="5" class="input-text" style="width: 100%; height: 80px;">Vull informació sobre la empresa "<?= $detail->nombre ?>"</textarea>
                            </div>
                            <div class="place-order mt-20">
                              <input type="hidden" name="empresas_id" value="<?= $detail->id ?>">
                              <input id="place_order" value="Enviar Solicitud" class="cws-button full-width alt" type="submit">
                            </div>
                          </div>
                        </div>

                      </div>
                  </form>
              </div>
          </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <!-- section reviews-->
                    <div id="reviews" class="mb-60">
                      <div class="row">
                        <div class="col-md-12">
                          <h4 class="trans-uppercase mb-10">Comentaris dels visitants</h4>
                          <div class="cws_divider mb-30"></div>
                        </div>
                      </div>
                      <div class="reviews-wrap">
                        <div class="comments">                         
                            <?php foreach($comentarios->result() as $c): ?>
                                  <div class="comment-body">
                                      <div class="avatar">
                                          <img src="<?= site_url() ?>pic/blog/90x90/1.jpg" data-at2x="pic/blog/90x90/1@2x.jpg" alt="">                    
                                      </div>
                                      <div class="comment-info">
                                        <div class="comment-meta">
                                          <div class="title">
                                            <h5><?php echo $c->titulo ?> <span><?php echo $c->nombre.' '.$c->apellido ?></span></h5>
                                          </div>
                                          <div class="comment-date">
                                              <span><?= dias(date("D",strtotime($c->fecha))).', '.date('d-m-Y',strtotime($c->fecha)) ?></span>
                                          </div>
                                        </div>
                                        <div class="comment-content">
                                          <p><?php echo $c->mensaje ?></p>
                                        </div>
                                      </div>
                                  </div>    
                            <?php endforeach ?>                                   
                        </div>            
                      </div>
                    </div>
                    <!-- review -->
                    
                    <div class="row">
                      <div class="col-md-12">
                        <h4 class="trans-uppercase mb-10">Escriu el teu missatge</h4>
                        <div class="cws_divider mb-30"></div>
                      </div>
                    </div>
                    <div class="review-content pattern relative">
                      <div class="row">
                        <div class="col-md-5 mb-md-30 mb-xs-0">
                          <div class="review-total">
                              <div style="background:url(<?= base_url('img/empresas/'.$detail->portada) ?>) repeat scroll center center / cover ; width: 120px; height: 120px; display: inline-block; float: left; margin-bottom: 30px;"></div>
                              <div class="review-total-content">
                                <h6>Destino <?php echo $detail->nombre ?></h6>
                              </div>
                          </div>
                        </div>              
                      </div>
                      <form class="form clearfix" onsubmit="return enviarComentario(this)">
                        <div class="row">
                          <div class="col-md-4">
                            <input type="text" name="nombre" value="" size="40" placeholder="Nom" aria-required="true" class="form-row form-row-first">
                          </div>
                          <div class="col-md-4">
                            <input type="text" name="apellido" value="" size="40" placeholder="Cognoms" aria-required="true" class="form-row form-row-first">
                          </div>
                          <div class="col-md-4">
                            <input type="text" name="telefono" value="" size="40" placeholder="Telèfon" aria-required="true" class="form-row form-row-first">
                          </div>
                          <div class="col-md-12">
                            <input type="text" name="titulo" value="" size="40" placeholder="Títol" aria-required="true" class="form-row form-row-last">
                          </div>
                          <div class="col-md-12">
                            <textarea name="mensaje" cols="40" rows="4" placeholder="Comentari" aria-invalid="false" aria-required="true" class="mb-20"></textarea>
                            <input type="hidden" name="empresas_id" value="<?php echo $detail->id ?>">
                            <input id="guardar" value="Afegir comentari" class="cws-button alt float-right" type="submit">
                          </div>
                        </div>
                      </form>
                    </div>
                    <!-- ! review -->
                </div>
                <div class="col-md-4">
                    <h4 class="trans-uppercase mb-10">Empreses recomenades</h4>
                    <div class="cws_divider mb-30"></div>
                    <div class="row">
                        <?php if($recomendados->num_rows()==0): ?>
                            Lo sentimos pero no existen empresas recomendados.
                        <?php endif ?>
                        <?php foreach($recomendados->result() as $r): ?>
                            <div class="col-md-12">
                                <div class="recom-item border">
                                    <div class="recom-media">
                                        <a href="<?= $r->link ?>">
                                            <div class="pic">
                                                <img src="<?= $r->foto ?>" data-at2x="<?= $r->foto ?>" alt="" style="width: 720px;">
                                            </div>
                                        </a>
                                        <div class="location">
                                            <?= $r->empresa ?>            
                                        </div>
                                    </div>
                                    <!-- Recomended Content-->
                                    <div class="recom-item-body">
                                        <a href="<?= $r->link ?>">
                                            <h6 class="blog-title"><?= $r->nombre ?></h6>
                                        </a>
                                        <p class="mb-30"><?= substr($r->descripcion_corta,0,80) ?></p>
                                        <a href="<?= $r->link ?>" class="recom-button">Leer más</a>
                                    </div>
                                    <!-- Recomended Image-->
                                </div>
                            </div>
                        <?php endforeach ?>
                    </div>
                </div>
            </div>
        </div>
        
      </section>
    </div>
<script src="//maps.googleapis.com/maps/api/js?key=AIzaSyBAmpz-tft5eyIVdHRZrXcEhj4ykyaC80E&libraries=drawing,places"></script>
<script>
    
    function mapa(){
        //Inicializar mapa    
        var mapaContent = document.getElementById('map');
        var center = new google.maps.LatLng<?php echo $detail->mapa ?>;    
        var mapOptions = {zoom: 12,center: center};
        var map = new google.maps.Map(mapaContent, mapOptions);
        new google.maps.Marker({position: center,map: map});
    }
    
    function enviarComentario(form){
        var f = new FormData(form);
        $("#guardar").attr('disabled',true);
        $.ajax({
            url:'<?php echo base_url('productos/frontend/comentario_empresa') ?>',
            data: f,
            context: document.body,
            cache: false,
            contentType: false,
            processData: false,
            type: 'POST',
            success:function(data){
                emergente(data);
            }
        });
        return false;
    }
    
    function reservar(form){
        var f = new FormData(form);
        $("#place_order").attr('disabled',true);
        $.ajax({
            url:'<?php echo base_url('productos/frontend/reservar') ?>',
            data: f,
            context: document.body,
            cache: false,
            contentType: false,
            processData: false,
            type: 'POST',
            success:function(data){
                emergente(data);
            }
        });
        return false;
    }
    mapa(); 
</script>