<header><?php $this->
load->view('includes/template/header'); ?> 
<!-- breadcrumbs start-->
<section style="background-image: url('http://arrelsnostres.cat/new/pic/breadcrumbs/bg-1.jpg'); background-repeat: no-repeat; background-position: center;" class="breadcrumbs" data-mce-style="background-image: url('http://arrelsnostres.cat/new/pic/breadcrumbs/bg-1.jpg'); background-repeat: no-repeat; background-position: center;">
<div class="container">
	<div class="text-left breadcrumbs-item">
		<a href="#">Inici</a><i>/</i><a href="#" class="last"><span>Serveis</span></a>
		<h2><span>SERV</span>EIS</h2>
	</div>
</div>
</section>
<!-- ! breadcrumbs end-->
</header>
<!-- ! header page-->
<div class="content-body">
	<!-- section features-->
	<section class="page-section pb-70">
	<div class="container">
		<div class="row">
			<div class="col-md-8">
				<h6 class="title-section-top font-4">QUE OFERIM A<br>
				</h6>
				<h2 class="title-section"><span>ARRELS NOSTRES</span></h2>
				<div class="cws_divider mb-25 mt-5">
					<br>
				</div>
				<p>
					A Arrels Nostres nos ponemos a su disposición para hacer de su viaje una gran experiencia.
				</p>
			</div>
		</div>
		<div class="row">
			<!-- service item-->
			<div class="col-sm-4 mb-40">
				<div class="service-item icon-right color-icon">
					<i class="flaticon-suntour-world cws-icon type-1 color-2"></i>
					<h3>Información de reserva de su viaje<br>
					</h3>
					<p class="mb-0">
						A través de nuestros asistentes, nuestras oficina, teléfono de contacto y mail.
					</p>
				</div>
			</div>
			<!-- service item-->
			<!-- ! service item-->
			<div class="col-sm-4 mb-40">
				<div class="service-item icon-right color-icon">
					<i class="flaticon-suntour-fireworks cws-icon type-1 color-2"></i>
					<h3 style="font-family: 'Hind Siliguri', sans-serif;" data-mce-style="font-family: 'Hind Siliguri', sans-serif;">Gestión de rooming-list<br>
					</h3>
					<p class="mb-0">
						Gestión del room list y gestiones con el hotel, asesoramiento, check in y check out.<br>
					</p>
				</div>
			</div>
			<!-- service item-->
			<!-- ! service item-->
			<div class="col-sm-4 mb-40">
				<div class="service-item icon-right color-icon">
					<i class="flaticon-suntour-hotel cws-icon type-1 color-2"></i>
					<h3 style="font-family: 'Hind Siliguri', sans-serif;" data-mce-style="font-family: 'Hind Siliguri', sans-serif;">Traslados y embarques ida y vuelta<br>
					</h3>
					<p class="mb-0">
						Traslados en destino siempre auxiliados por personal Kanvoy.<br>
					</p>
				</div>
			</div>
			<!-- ! service item-->
			<!-- service item-->
			<div class="col-sm-4 mb-40">
				<div class="service-item icon-right color-icon">
					<i class="flaticon-suntour-ship cws-icon type-1 color-2"></i>
					<h3 style="font-family: 'Hind Siliguri', sans-serif;" data-mce-style="font-family: 'Hind Siliguri', sans-serif;">Alojamiento y actividades<br>
					</h3>
					<p class="mb-0">
						Garantizados y reservados con los mejores hoteles y actividades.<br>
					</p>
				</div>
			</div>
			<!-- service item-->
			<!-- ! service item-->
			<div class="col-sm-4 mb-40">
				<div class="service-item icon-right color-icon">
					<i class="flaticon-suntour-airplane cws-icon type-1 color-2"></i>
					<h3 style="font-family: 'Hind Siliguri', sans-serif;" data-mce-style="font-family: 'Hind Siliguri', sans-serif;">Gestión y producción merchandising</h3>
					<p class="mb-0">
						Buscamos y confeccionamos los productos que os interesen, y los adaptamos a vuestras peticiones.
					</p>
				</div>
			</div>
			<!-- service item-->
			<!-- ! service item-->
			<div class="col-sm-4">
				<div class="service-item icon-right color-icon">
					<i class="flaticon-suntour-car cws-icon type-1 color-2"></i>
					<h3 style="font-family: 'Hind Siliguri', sans-serif;" data-mce-style="font-family: 'Hind Siliguri', sans-serif;">Y más...<br>
					</h3>
					<p class="mb-0">
						Y un largo etc, que hará que vuestro viaje sea &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; inolvidable.<br>
					</p>
				</div>
			</div>
			<!-- ! service item-->
		</div>
	</div>
	</section>
	<!-- ! section features-->
	<!-- counter section -->
	<section class="small-section cws_prlx_section bg-gray-60"><img src="<?=base_url() ?>pic/parallax-1.jpg" alt="" class="cws_prlx_layer">
	<div class="container">
		<div class="row">
			<div class="col-md-2 col-xs-6 mb-md-30">
				<div class="counter-block white">
					<i class="counter-icon flaticoncustom-draw"></i>
					<div class="counter-name-wrap">
						<div data-count="345" class="counter">
							0
						</div>
						<div class="counter-name">
							Empreses<br>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-2 col-xs-6 mb-md-30">
				<div class="counter-block with-divider white">
					<i class="counter-icon flaticoncustom-hands"></i>
					<div class="counter-name-wrap">
						<div data-count="438" class="counter">
							0
						</div>
						<div class="counter-name">
							Productes<br>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-2 col-xs-6 mb-md-30">
				<div class="counter-block with-divider white">
					<i class="counter-icon flaticoncustom-social"></i>
					<div class="counter-name-wrap">
						<div data-count="526" class="counter">
							0
						</div>
						<div class="counter-name">
							Sectors
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-2 col-xs-6 mb-md-30">
				<div class="counter-block with-divider white">
					<i class="counter-icon flaticoncustom-commerce"></i>
					<div class="counter-name-wrap">
						<div data-count="169" class="counter">
							0
						</div>
						<div class="counter-name">
							Repercusió
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-2 col-xs-6">
				<div class="counter-block with-divider white">
					<i class="counter-icon flaticoncustom-global"></i>
					<div class="counter-name-wrap">
						<div data-count="293" class="counter">
							0
						</div>
						<div class="counter-name">
							Clients<br>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-2 col-xs-6">
				<div class="counter-block with-divider white">
					<i class="counter-icon flaticoncustom-business"></i>
					<div class="counter-name-wrap">
						<div data-count="675" class="counter">
							0
						</div>
						<div class="counter-name">
							Facturació<br>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	</section>
	<!-- ! counter section-->
	<!-- page section about-->
	<section class="page-section">
	<div class="container">
		<div class="row">
			<div class="col-md-6 mb-md-50">
				<img src="<?=base_url() ?>pic/promo-2.jpg" alt="" class="mt-minus-100">
			</div>
			<div class="col-md-6">
				<!-- section title-->
				<h2 class="title-section mt-0 mb-0">COM HO FEM?<br>
				</h2>
				<!-- ! section title-->
				<div class="cws_divider with-plus short-3 mb-20 mt-10">
					<br>
				</div>
				<p class="mb-50">
					Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim.
				</p>
				<!-- accordion-->
				<div class="accordion mb-50">
					<div class="content-title active">
						<span class="active"><i class="active-icon"></i>Ut ultrices justo quis nulla vulputate iaculis</span>
					</div>
					<div class="content">
						In mattis ut dui sed viverra. Nam eleifend libero ligula, eget imperdiet neque dapibus id. Integer sit amet dolor vel eros laoreet dignissim. Nullam in mauris turpis. Cras velit risus, mollis et tortor convallis, ornare interdum lorem. Donec pharetra odio luctus, porttitor tortor vel, mattis ante.
					</div>
					<div class="content-title">
						<span><i class="active-icon"></i>Sed volutpat tellus a odio laoreet imperdiet</span>
					</div>
					<div class="content">
						In mattis ut dui sed viverra. Nam eleifend libero ligula, eget imperdiet neque dapibus id. Integer sit amet dolor vel eros laoreet dignissim. Nullam in mauris turpis. Cras velit risus, mollis et tortor convallis, ornare interdum lorem. Donec pharetra odio luctus, porttitor tortor vel, mattis ante.
					</div>
					<div class="content-title">
						<span><i class="active-icon"></i>Donec at massa eu lacus accumsan semper</span>
					</div>
					<div class="content">
						In mattis ut dui sed viverra. Nam eleifend libero ligula, eget imperdiet neque dapibus id. Integer sit amet dolor vel eros laoreet dignissim. Nullam in mauris turpis. Cras velit risus, mollis et tortor convallis, ornare interdum lorem. Donec pharetra odio luctus, porttitor tortor vel, mattis ante.
					</div>
				</div>
				<!-- ! accordion-->
				<a href="#" class="cws-button alt">CONTACTAR</a><a href="#" class="cws-button alt gray-dark">SUPORT ONLINE</a>
			</div>
		</div>
	</div>
	<!-- list-->
	</section>
	<!-- ! page section about-->
	<!-- page services-->
	<section class="page-section cws_prlx_section pb-100 bg-gray-60"><img src="<?=base_url() ?>pic/parallax-3.jpg" alt="" class="cws_prlx_layer">
	<div class="container">
		<div class="row">
			<div class="col-md-8">
				<h2 class="title-section alt"><span>ELS NOSTRES</span>&nbsp;SERVEIS</h2>
				<div class="cws_divider mb-25 mt-5">
					<br>
				</div>
				<p class="color-white">
					Vestibulum feugiat vitae tortor ut venenatis. Sed cursus, purus eu euismod bibendum, diam nisl suscipit odio, vitae ultrices mauris dolor quis mauris. Curabitur ac metus id leo maximus porta.
				</p>
			</div>
		</div>
		<div class="row">
			<!-- service item-->
			<div class="col-md-4 col-sm-6 mb-40">
				<div class="service-item icon-center color-icon border">
					<i class="flaticon-suntour-world cws-icon type-1 color-2"></i>
					<h3>Global Locations</h3>
					<p class="mb-0">
						Donec sodales sagittis magna. Sed consequat, leo eget bibendum sodales.
					</p>
				</div>
			</div>
			<!-- service item-->
			<!-- ! service item-->
			<div class="col-md-4 col-sm-6 mb-40">
				<div class="service-item icon-center color-icon border">
					<i class="flaticon-suntour-fireworks cws-icon type-1 color-2"></i>
					<h3>Entertainment</h3>
					<p class="mb-0">
						Cras dapibus, vamus elementum semper nisi. Aenean vulputate eleifend tellus.
					</p>
				</div>
			</div>
			<!-- service item-->
			<!-- ! service item-->
			<div class="col-md-4 col-sm-6 mb-40">
				<div class="service-item icon-center color-icon border">
					<i class="flaticon-suntour-hotel cws-icon type-1 color-2"></i>
					<h3>Info &amp; Guides</h3>
					<p class="mb-0">
						In enim justo, rhoncus ut, imperdiet a, venenatis doesi aer vitae, justo.
					</p>
				</div>
			</div>
			<!-- ! service item-->
			<!-- service item-->
			<div class="col-md-4 col-sm-6 mb-40">
				<div class="service-item icon-center color-icon border">
					<i class="flaticon-suntour-ship cws-icon type-1 color-2"></i>
					<h3>Ship Tickets</h3>
					<p class="mb-0">
						Cum sociis natoque penatibus et magnis dis partu rien montes, nascetur ridiculus.
					</p>
				</div>
			</div>
			<!-- service item-->
			<!-- ! service item-->
			<div class="col-md-4 col-sm-6 mb-40">
				<div class="service-item icon-center color-icon border">
					<i class="flaticon-suntour-airplane cws-icon type-1 color-2"></i>
					<h3>Flight Tickets</h3>
					<p class="mb-0">
						Fusce commodo aliquam arcu. Nam commodo suscipit quam. Quisque id odio.
					</p>
				</div>
			</div>
			<!-- service item-->
			<!-- ! service item-->
			<div class="col-md-4 col-sm-6">
				<div class="service-item icon-center color-icon border">
					<i class="flaticon-suntour-car cws-icon type-1 color-2"></i>
					<h3>Car Tickets</h3>
					<p class="mb-0">
						Praesent ac sem eget est egestas volutpat. Vivamus consectetuer hendrerit.
					</p>
				</div>
			</div>
			<!-- ! service item-->
		</div>
	</div>
	</section>
	<!-- ! page services-->
	<!-- call out section-->
	<?php $this->
	load->view('includes/template/subscribe'); ?> <section class="page-section cws_prlx_section bg-white-80 pb-60 pt-60" id="cws_prlx_section_1063912445238"><img src="http://arrelsnostres.cat/new/pic/parallax-4.jpg" alt="" class="cws_prlx_layer" id="cws_prlx_layer_213049917628" style="transform: translate(-50%, -243.169px);">
	<div class="container">
		<div class="call-out-box">
			<div class="call-out-wrap alt">
				<h2 class="title-section alt-2 gray">TENS ALGUNA PREGUNTA?</h2>
				<a href="http://kanvoy.com/p/contacto" class="cws-button border-left large alt mb-20">CONTACTAR</a>
			</div>
		</div>
	</div>
	</section>