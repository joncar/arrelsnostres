<header><?php $this->
load->view('includes/template/header'); ?> 
<!-- breadcrumbs start-->
<section style="background-image: url('http://kanvoy.com/pic/breadcrumbs/bg-1.jpg'); background-repeat: no-repeat; background-position: center;" class="breadcrumbs" data-mce-style="background-image: url('http://kanvoy.com/pic/breadcrumbs/bg-1.jpg'); background-repeat: no-repeat; background-position: center;">
<div class="container">
	<div class="text-left breadcrumbs-item">
		<a href="#">Inici</a><i>/</i><a href="#" class="last"><span>Contacte</span></a>
		<h2><span>CONT</span>ACTE</h2>
	</div>
</div>
</section>
<!-- ! breadcrumbs end-->
</header>
<div class="content-body">
	<div class="container page">
		<div class="row">
			<div class="col-md-6">
				<div class="contact-item">
					<h4 class="title-section mt-30"><span class="font-bold">Dades de contacte<br>
					</span></h4>
					<div class="cws_divider mb-25 mt-5">
						<br>
					</div>
					<ul class="icon">
						<li><a href="#">info@arrelsnostres.cat<i class="flaticon-suntour-email"></i></a></li>
						<li><a href="#">93 805 3738 <i class="flaticon-suntour-phone"></i></a></li>
						
						<li><a href="#">C/ President Lluis Companys 28. 08700 Igualada / Barcelona<i class="flaticon-suntour-map"></i></a></li>
					</ul>
					<p class="mt-20">
						<strong>Horari de Atenció al Client </strong>
					</p>
					<p class="mt-20">
						DILLUNS A DIVENDRES DE 10:00 A 13:30H.&nbsp;Y DE 17:00 A 20:00H.
					</p>
					<div class="contact-cws-social">
						<a href="#" class="cws-social fa fa-twitter"></a><a href="#" class="cws-social fa fa-facebook"></a><a href="#" class="cws-social fa fa-google-plus"></a><a href="#" class="cws-social fa fa-linkedin"></a><br>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="map-wrapper">
					<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2983.9292136805075!2d1.6113857161637493!3d41.59241407924589!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x12a4420bd3268113%3A0x5991e117829f502b!2sHIPO+(e%2Bd+jordi+maga%C3%B1a%2Csl)!5e0!3m2!1ses!2ses!4v1489053729767" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
				</div>
			</div>
		</div>
	</div>
	<div class="element-section pattern bg-gray-3 relative pt-60 pb-100">
		<div class="container">
			<h4 class="title-section mb-20"><span class="font-bold">Contacte amb nosaltres</span></h4>
			<div class="widget-contact-form pb-0">
				<!-- contact-form-->
				<div class="email_server_responce">
				</div>
				<form method="post" class="form contact-form alt clearfix" onsubmit="return contacto(this)">
					<div class="row">
						<div class="col-md-6 clearfix">
							<div class="input-container">
								<input name="nombre" value="" size="40" placeholder="Nom" aria-invalid="false" aria-required="true" class="form-row form-row-first" type="text">
							</div>
						</div>
						<div class="col-md-6">
							<div class="input-container">
								<input name="email" value="" size="40" placeholder="Email" aria-required="true" class="form-row form-row-last" type="text">
							</div>
						</div>
					</div>
					<div class="input-container">
						<textarea name="comentario" cols="40" rows="4" placeholder="Comentari" aria-invalid="false" aria-required="true"></textarea>
					</div>
					<input id="guardar" value="Contactar" class="cws-button alt" type="submit">
				</form>
				<!-- /contact-form-->
			</div>
		</div>
	</div>
</div>
<script>
    function contacto(form){
        var f = new FormData(form);
        $("#guardar").attr('disabled',true);
        $.ajax({
            url:'<?php echo base_url('paginas/frontend/contacto') ?>',
            data: f,
            context: document.body,
            cache: false,
            contentType: false,
            processData: false,
            type: 'POST',
            success:function(data){
                emergente(data);
            }
        });
        return false;
    }
</script>