<header><?php $this->
load->view('includes/template/header');
?></header>
<!-- ! header page-->
<div class="content-body">
    <div class="tp-banner-container">
        <div class="tp-banner-slider">
            <ul>
                <li data-masterspeed="700" data-slotamount="7" data-transition="fade" style="margin-left: 0px; padding-left: 0px;" data-mce-style="margin-left: 0px; padding-left: 0px;"><img src="http://kanvoy.com/rs-plugin/assets/loader.gif" data-lazyload="pic/slider/main/slide-1.jpg" data-bgposition="center" alt="" data-kenburns="on" data-duration="30000" data-ease="Linear.easeNone" data-scalestart="100" data-scaleend="120" data-rotatestart="0" data-rotateend="0" data-offsetstart="0 0" data-offsetend="0 0" data-bgparallax="10">
                    <div data-x="['center','center','center','center']" data-y="center" data-transform_in="x:-150px;opacity:0;s:1500;e:Power3.easeInOut;" data-transform_out="x:150px;opacity:0;s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" data-start="400" class="tp-caption sl-content">
                        <div class="sl-title-top">
                            Arrels Nostres
                        </div>
                        <div class="sl-title">
                            Can Mabres
                        </div>
                        <div class="sl-title-bot">
                            Carn <span>de</span> qualitat
                        </div>
                    </div>
                </li>
                <li data-masterspeed="700" data-transition="fade" style="margin-left: 0px; padding-left: 0px;" data-mce-style="margin-left: 0px; padding-left: 0px;"><img src="http://kanvoy.com/rs-plugin/assets/loader.gif" data-lazyload="pic/slider/main/slide-2.jpg" data-bgposition="center" alt="" data-kenburns="on" data-duration="30000" data-ease="Linear.easeNone" data-scalestart="100" data-scaleend="120" data-rotatestart="0" data-rotateend="0" data-offsetstart="0 0" data-offsetend="0 0" data-bgparallax="10">
                    <div data-x="['center','center','center','center']" data-y="center" data-transform_in="y:-150px;opacity:0;s:1500;e:Power3.easeInOut;" data-transform_out="y:150px;opacity:0;s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" data-start="400" class="tp-caption sl-content">
                        <div class="sl-title-top">
                            Arrels Nostres
                        </div>
                        <div class="sl-title">
                            Caves Bohigas
                        </div>
                        <div class="sl-title-bot">
                            Caves <span>amb</span> personalitat
                        </div>
                    </div>
                </li>
                <li data-masterspeed="700" data-transition="fade" style="margin-left: 0px; padding-left: 0px;" data-mce-style="margin-left: 0px; padding-left: 0px;"><img src="http://kanvoy.com/rs-plugin/assets/loader.gif" data-lazyload="pic/slider/main/slide-3.jpg" data-bgposition="center" alt="" data-kenburns="on" data-duration="30000" data-ease="Linear.easeNone" data-scalestart="100" data-scaleend="120" data-rotatestart="0" data-rotateend="0" data-offsetstart="0 0" data-offsetend="0 0" data-bgparallax="10">
                    <div data-x="['center','center','center','center']" data-y="center" data-transform_in="x:150px;opacity:0;s:1500;e:Power3.easeInOut;" data-transform_out="x:-150px;opacity:0;s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" data-start="400" class="tp-caption sl-content">
                        <div class="sl-title-top">
                            Arrels Nostres
                        </div>
                        <div class="sl-title">
                            Fruits secs Sant Jordi
                        </div>
                        <div class="sl-title-bot">
                            El sabor<span>de</span> la tradició
                        </div>
                    </div>
                </li>
                <li data-masterspeed="700" data-transition="fade" style="margin-left: 0px; padding-left: 0px;" data-mce-style="margin-left: 0px; padding-left: 0px;"><img src="http://kanvoy.com/rs-plugin/assets/loader.gif" data-lazyload="pic/slider/main/slide-4.jpg" data-bgposition="center" alt="" data-kenburns="on" data-duration="30000" data-ease="Linear.easeNone" data-scalestart="100" data-scaleend="120" data-rotatestart="0" data-rotateend="0" data-offsetstart="0 0" data-offsetend="0 0" data-bgparallax="10">
                    <div data-x="['center','center','center','center']" data-y="center" data-transform_in="x:150px;opacity:0;s:1500;e:Power3.easeInOut;" data-transform_out="x:-150px;opacity:0;s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" data-start="400" class="tp-caption sl-content">
                        <div class="sl-title-top">
                            Arrels Nostres
                        </div>
                        <div class="sl-title">
                            Salses Fruits S&amp;P
                        </div>
                        <div class="sl-title-bot">
                            Salses <span>amb</span> molt de gust
                        </div>
                    </div>
                </li>
            </ul>
        </div>
        <!-- slider info-->
        <div class="slider-info-wrap clearfix">
            <div class="slider-info-content">
                <div class="slider-info-item">
                    <div class="info-item-media " style="border-bottom: 4px solid #8e5253;" data-mce-style="border-bottom: 4px solid #8e5253;">
                        <img src="pic/slider-info-1.jpg" data-at2x="pic/slider-info-1@2x.jpg" alt="">
                        <div class="info-item-text">
                            <div class="info-price font-4">
                                <span>L'anoia<br>
                                </span>Castellolí<br>
                            </div>
                            <div class="info-temp font-4">
                                <span>PRODUCTES</span>10
                            </div>
                            <p class="info-text">

                            </p>
                        </div>
                    </div>
                    <div class="info-item-content">
                        <div class="main-title">
                            <h3 class="title"><span class="font-4">CARN DE QUALITAT<br>
                                </span>CAN MABRES</h3>
                            <a href="<?= base_url() ?>empresa/4-carn-can-mabres.html" class="button">VEURE</a>
                        </div>
                    </div>
                </div>
                <div class="slider-info-item">
                    <div class="info-item-media " style="border-bottom: 4px solid #a9b674;" data-mce-style="border-bottom: 4px solid #a9b674;">
                        <img src="pic/slider-info-2.jpg" data-at2x="pic/slider-info-2@2x.jpg" alt="">
                        <div class="info-item-text">
                            <div class="info-price font-4">
                                <span>L'ANOIA</span>Òdena
                            </div>
                            <div class="info-temp font-4">
                                <span>PRODUCTES</span>10
                            </div>
                            <p class="info-text">

                            </p>
                        </div>
                    </div>
                    <div class="info-item-content">
                        <div class="main-title">
                            <h3 class="title"><span class="font-4">CAVES AMB PERSONALITAT&nbsp;</span>CAVES BOHIGAS<br>
                            </h3>
                            <a href="<?= base_url() ?>empresa/3-caves-bohigas.html" class="button">VEURE</a>
                        </div>
                    </div>
                </div>
                <div class="slider-info-item">
                    <div class="info-item-media " style="border-bottom: 4px solid #e2c786;" data-mce-style="border-bottom: 4px solid #e2c786;">
                        <img src="pic/slider-info-3.jpg" data-at2x="pic/slider-info-3@2x.jpg" alt="">
                        <div class="info-item-text">
                            <div class="info-price font-4">
                                <span>L'Anoia</span>Jorba<br>
                            </div>
                            <div class="info-temp font-4">
                                <span>PRODUCTES</span>10
                            </div>
                            <p class="info-text">

                            </p>
                        </div>
                    </div>
                    <div class="info-item-content">
                        <div class="main-title">
                            <h3 class="title"><span class="font-4">el sabor de la tradició</span>FRUITS SECS SANT JOrdi</h3>
                            <a href="<?= base_url() ?>empresa/2-fruit-secs-sant-jordi.html" class="button">VEURE</a>
                        </div>
                    </div>
                </div>
                <div class="slider-info-item">
                    <div class="info-item-media " style="border-bottom: 4px solid #eed659;" data-mce-style="border-bottom: 4px solid #eed659;">
                        <img src="pic/slider-info-4.jpg" data-at2x="pic/slider-info-4@2x.jpg" alt="">
                        <div class="info-item-text">
                            <div class="info-price font-4">
                                <span>ALT CAMP<br>
                                </span>Figuerola del Camp<br>
                            </div>
                            <div class="info-temp font-4">
                                <span>PRODUCTEs</span>10
                            </div>
                            <p class="info-text">

                            </p>
                        </div>
                    </div>
                    <div class="info-item-content">
                        <div class="main-title">
                            <h3 class="title"><span class="font-4">salses amb molt de gust&nbsp;<br>
                                </span>salses fruits s&amp;P<br>
                            </h3>
                            <a href="<?= base_url() ?>empresa/1-salses-fruits-s-p.html" class="button">VEURE</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- ! slider-info-->
    </div>
    <!-- page section-->
    <section class="page-section pb-0">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <h6 class="title-section-top font-4">ARRELS NOSTRES</h6>
                    <h2 class="title-section"><span>EL QUÈ, COM</span>&nbsp;I PERQUÈ?</h2>
                    <div class="cws_divider mb-25 mt-5">
                        <br>
                    </div>
                    <p>
                        Entra i coneix qui som, com fem els nostres productes i la nostra filosofia...
                    </p>
                    <div>
                        <img src="pic/logos.jpg" data-at2x="pic/logos@2x.jpg" style="margin-bottom: 20px;" data-mce-style="margin-bottom: 20px;">
                    </div>
                </div>
                <div class="col-md-4">
                    <img src="pic/promo-1.png" data-at2x="pic/promo-1@2x.png" alt="" class="mt-md-0 mt-minus-70">
                </div>
            </div>
        </div>
        <div class="features-tours-full-width">
            <div class="features-tours-wrap clearfix">
                <?php foreach ($this->
                db->get('empresas')->result() as $cat): $this->db->select('productos.*, empresas.nombre, categorias.empresas_id');
                    $this->db->join('categorias', 'categorias.id = productos.categorias_id');
                    $this->db->join('empresas', 'empresas.id = categorias.empresas_id');
                    $this->db->limit('2');
                    $this->db->order_by('visitas', 'DESC');
                    $productos = $this->db->get_where('productos', array('empresas_id' => $cat->id));
                    foreach ($productos->result() as $d):
                        ?>
                        <div class="features-tours-item">
                            <div class="features-media">
                                <img style="width: 480px;" src="<?= base_url('img/productos/' . $d->portada_main) ?>" data-at2x="<?= base_url('img/productos/' . $d->
                                portada_main)
                        ?>" alt="" data-mce-style="width: 480px;">
                                <div class="features-info-top">
                                    <div class="info-price font-4">
                                        <span>Procedencia</span><?= $d->
                                procedencia
                                ?>
                                    </div>
                                    <div class="info-temp font-4">
                                        <span>Mesura </span><?= $d->
                                mesura
                        ?>
                                    </div>
                                    <p class="info-text">
                        <?= substr(strip_tags($d->
                                        descripcion_corta), 0, 255)
                        ?>
                                    </p>
                                </div>
                                <div class="features-info-bot">
                                    <h4 class="title"><span class="font-4"><?= $d->
                nombre
                ?></span><?= $d->
                productos_nombre
                ?></h4>
                                    <a href="<?= site_url('empresa/' . toURL($d->empresas_id . '-' . $d->nombre)) ?>#productos" class="button">Veure</a>
                                </div>
                            </div>
                        </div>
    <?php endforeach ?>
<?php endforeach ?>
            </div>
        </div>
    </section>
    <!-- ! testimonials section-->
    <section class="small-section">
        <div class="container">
            <div class="row">
                <div class="col-md-2 col-xs-6 mb-md-30">
                    <div class="counter-block">
                        <i class="counter-icon flaticoncustom-draw"></i>
                        <div class="counter-name-wrap">
                            <div data-count="345" class="counter">
                                345
                            </div>
                            <div class="counter-name">
                                Empreses<br>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-2 col-xs-6 mb-md-30">
                    <div class="counter-block with-divider">
                        <i class="counter-icon flaticoncustom-hands"></i>
                        <div class="counter-name-wrap">
                            <div data-count="438" class="counter">
                                438
                            </div>
                            <div class="counter-name">
                                Productes<br>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-2 col-xs-6 mb-md-30">
                    <div class="counter-block with-divider">
                        <i class="counter-icon flaticoncustom-social"></i>
                        <div class="counter-name-wrap">
                            <div data-count="526" class="counter">
                                526
                            </div>
                            <div class="counter-name">
                                Sectors<br>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-2 col-xs-6 mb-md-30">
                    <div class="counter-block with-divider">
                        <i class="counter-icon flaticoncustom-commerce"></i>
                        <div class="counter-name-wrap">
                            <div data-count="169" class="counter">
                                169
                            </div>
                            <div class="counter-name">
                                Repercusió
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-2 col-xs-6">
                    <div class="counter-block with-divider">
                        <i class="counter-icon flaticoncustom-global"></i>
                        <div class="counter-name-wrap">
                            <div data-count="293" class="counter">
                                293
                            </div>
                            <div class="counter-name">
                                Clients<br>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-2 col-xs-6">
                    <div class="counter-block with-divider">
                        <i class="counter-icon flaticoncustom-business"></i>
                        <div class="counter-name-wrap">
                            <div data-count="675" class="counter">
                                675
                            </div>
                            <div class="counter-name">
                                Facturació<br>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section><section class="small-section cws_prlx_section bg-blue-40"><img src="pic/parallax-2.jpg" alt="" class="cws_prlx_layer">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <h6 class="title-section-top font-4">gent feliç</h6>
                    <h2 class="title-section alt-2"><span>els clients</span> opinen</h2>
                    <div class="cws_divider mb-25 mt-5">
                        <br>
                    </div>
                </div>
            </div>
            <div class="row">
                <!-- testimonial carousel-->
                <div class="owl-three-item">
                    <!-- testimonial item-->
                    <div class="testimonial-item">
                        <div class="testimonial-top">
                            <div class="pic">
                                <img src="pic/testimonial/top-bg/1.jpg" data-at2x="pic/testimonial/top-bg/1@2x.jpg" alt="">
                            </div>
                            <div class="author">
                                <img src="pic/testimonial/author/1.jpg" data-at2x="pic/testimonial/author/1@2x.jpg" alt="">
                            </div>
                        </div>
                        <!-- testimonial content-->
                        <div class="testimonial-body">
                            <h5 class="title"><span>Jordi</span> Magaña</h5>
                            <div class="stars stars-5">
                                <br>
                            </div>
                            <p class="align-center">
                                A casa no tenim mai temps de cuinar i amb els productes de Salses Fruits S&P mengem cada dia productes frescos! Ens encanta!
                            </p>
                            <a href="http://kanvoy.com/p/nosotros.html#testimonios" class="testimonial-button">Girona</a>
                        </div>
                    </div>
                    <!-- testimonial item-->
                    <div class="testimonial-item">
                        <div class="testimonial-top">
                            <div class="pic">
                                <img src="pic/testimonial/top-bg/2.jpg" data-at2x="pic/testimonial/top-bg/2@2x.jpg" alt="">
                            </div>
                            <div class="author">
                                <img src="pic/testimonial/author/2.jpg" data-at2x="pic/testimonial/author/2@2x.jpg" alt="">
                            </div>
                        </div>
                        <!-- testimonial content-->
                        <div class="testimonial-body">
                            <h5 class="title"><span>Pep</span> Soler</h5>
                            <div class="stars stars-5">
                                <br>
                            </div>
                            <p class="align-center">
                                Al nostre territori tenim caves d'altíssima qualitat. Pel meu casament vam brindar amb cava Bohigas i des d'aleshores el consumin a casa.
                            </p>
                            <a href="http://kanvoy.com/p/nosotros.html#testimonios" class="testimonial-button">Gurb</a>
                        </div>
                    </div>
                    <!-- testimonial item-->
                    <div class="testimonial-item">
                        <div class="testimonial-top">
                            <div class="pic">
                                <img src="pic/testimonial/top-bg/3.jpg" data-at2x="pic/testimonial/top-bg/3@2x.jpg" alt="">
                            </div>
                            <div class="author">
                                <img src="pic/testimonial/author/3.jpg" data-at2x="pic/testimonial/author/3@2x.jpg" alt="">
                            </div>
                        </div>
                        <!-- testimonial content-->
                        <div class="testimonial-body">
                            <h5 class="title"><span>Eliana</span> Tusell</h5>
                            <div class="stars stars-5">
                                <br>
                            </div>
                            <p class="align-center">
                                La carn de Can Mabres és la meva preferida, es desfà a la boca! Des d'aleshores no en menjo cap més, qualitat i proximitat!
                            </p>
                            <a href="http://kanvoy.com/p/nosotros.html#testimonios" class="testimonial-button">Valls</a>
                        </div>
                    </div>
                    <!-- testimonial item-->
                    <div class="testimonial-item">
                        <div class="testimonial-top">
                            <div class="pic">
                                <img src="http://kanvoy.com/pic/testimonial/top-bg/1.jpg" data-at2x="http://kanvoy.com/pic/testimonial/top-bg/1@2x.jpg" alt="">
                            </div>
                            <div class="author">
                                <img src="http://kanvoy.com/pic/testimonial/author/1.jpg" data-at2x="http://kanvoy.com/pic/testimonial/author/1@2x.jpg" alt="">
                            </div>
                        </div>
                        <!-- testimonial content-->
                        <div class="testimonial-body">
                            <h5 class="title"><span>Joan</span> Bernadí</h5>
                            <div class="stars stars-5">
                                <br>
                            </div>
                            <p class="align-center">
                                Soc molt de fruits secs, a les amanides, als vermuts, als plats cuinats.... Fruits Sant Jordi m'ofereix moltissima varietat i fruits molt gustosos. Des que els vaig conèixer no en compro cap més!
                            </p>
                            <a href="http://kanvoy.com/p/nosotros.html#testimonios" class="testimonial-button">Leer más</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- gallery section-->
    <section class="small-section">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <h6 class="title-section-top font-4">Imatges nostres</h6>
                    <h2 class="title-section"><span>GALERIA</span>&nbsp;DE FOTOS</h2>
                    <div class="cws_divider mb-25 mt-5">
                        <br>
                    </div>
                    <p>
                        Recull d'imatges dels nostres productes i allà on es creen.
                    </p>
                </div>
                <div class="col-md-4">
                    <i class="flaticon-suntour-photo title-icon"></i><br>
                </div>
            </div>
            <div class="row portfolio-grid">
                <!-- portfolio item-->
                <div class="col-md-6 col-sm-12 col-xs-12">
                    <div class="portfolio-item big">
                        <!-- portfolio image-->
                        <div class="portfolio-media">
                            <img src="pic/portfolio/580x285-1.jpg" data-at2x="pic/portfolio/580x285-1@2x.jpg" alt="">
                        </div>
                        <div class="links">
                            <a href="pic/portfolio/580x285-1@2x.jpg" class="fancy"><i class="fa fa-expand"></i></a><br>
                        </div>
                    </div>
                </div>
                <!-- portfolio item-->
                <div class="col-md-3 col-sm-6 col-xs-6">
                    <div class="portfolio-item">
                        <!-- portfolio image-->
                        <div class="portfolio-media">
                            <img src="pic/portfolio/285x285-1.jpg" data-at2x="pic/portfolio/285x285-1@2x.jpg" alt="">
                        </div>
                        <div class="links">
                            <a href="pic/portfolio/285x285-1@2x.jpg" class="fancy"><i class="fa fa-expand"></i></a><br>
                        </div>
                    </div>
                </div>
                <!-- portfolio item-->
                <div class="col-md-3 col-sm-6 col-xs-6">
                    <div class="portfolio-item">
                        <!-- portfolio image-->
                        <div class="portfolio-media">
                            <img src="pic/portfolio/285x285-2.jpg" data-at2x="pic/portfolio/285x285-2@2x.jpg" alt="">
                        </div>
                        <div class="links">
                            <a href="pic/portfolio/285x285-2@2x.jpg" class="fancy"><i class="fa fa-expand"></i></a><br>
                        </div>
                    </div>
                </div>
                <!-- portfolio item-->
                <div class="col-md-3 col-sm-6 col-xs-6">
                    <div class="portfolio-item">
                        <!-- portfolio image-->
                        <div class="portfolio-media">
                            <img src="pic/portfolio/285x285-3.jpg" data-at2x="pic/portfolio/285x285-3@2x.jpg" alt="">
                        </div>
                        <div class="links">
                            <a href="pic/portfolio/285x285-3@2x.jpg" class="fancy"><i class="fa fa-expand"></i></a><br>
                        </div>
                    </div>
                </div>
                <!-- portfolio item-->
                <div class="col-md-3 col-sm-6 col-xs-6">
                    <div class="portfolio-item">
                        <!-- portfolio image-->
                        <div class="portfolio-media">
                            <img src="pic/portfolio/285x285-4.jpg" data-at2x="pic/portfolio/285x285-4@2x.jpg" alt="">
                        </div>
                        <div class="links">
                            <a href="pic/portfolio/285x285-4@2x.jpg" class="fancy"><i class="fa fa-expand"></i></a><br>
                        </div>
                    </div>
                </div>
                <!-- portfolio item-->
                <div class="col-md-3 col-sm-6 col-xs-6">
                    <div class="portfolio-item">
                        <!-- portfolio image-->
                        <div class="portfolio-media">
                            <img src="pic/portfolio/285x285-5.jpg" data-at2x="pic/portfolio/285x285-5@2x.jpg" alt="">
                        </div>
                        <div class="links">
                            <a href="pic/portfolio/285x285-5@2x.jpg" class="fancy"><i class="fa fa-expand"></i></a><br>
                        </div>
                    </div>
                </div>
                <!-- portfolio item-->
                <div class="col-md-3 col-sm-6 col-xs-6">
                    <div class="portfolio-item">
                        <!-- portfolio image-->
                        <div class="portfolio-media">
                            <img src="pic/portfolio/285x285-6.jpg" data-at2x="pic/portfolio/285x285-6@2x.jpg" alt="">
                        </div>
                        <div class="links">
                            <a href="pic/portfolio/285x285-6@2x.jpg" class="fancy"><i class="fa fa-expand"></i></a><br>
                        </div>
                    </div>
                </div>
                <!-- portfolio item-->
                <div class="col-md-3 col-sm-6 col-xs-6">
                    <div class="portfolio-item">
                        <!-- portfolio image-->
                        <div class="portfolio-media">
                            <img src="pic/portfolio/285x285-7.jpg" data-at2x="pic/portfolio/285x285-7@2x.jpg" alt="">
                        </div>
                        <div class="links">
                            <a href="pic/portfolio/285x285-7@2x.jpg" class="fancy"><i class="fa fa-expand"></i></a><br>
                        </div>
                    </div>
                </div>
                <!-- portfolio item-->
                <div class="col-md-3 col-sm-6 col-xs-6">
                    <div class="portfolio-item">
                        <!-- portfolio image-->
                        <div class="portfolio-media">
                            <img src="pic/portfolio/285x285-8.jpg" data-at2x="pic/portfolio/285x285-8@2x.jpg" alt="">
                        </div>
                        <div class="links">
                            <a href="pic/portfolio/285x285-8@2x.jpg" class="fancy"><i class="fa fa-expand"></i></a><br>
                        </div>
                    </div>
                </div>
                <!-- portfolio item-->
                <div class="col-md-6 col-sm-12 col-xs-12">
                    <div class="portfolio-item big">
                        <!-- portfolio image-->
                        <div class="portfolio-media">
                            <img src="pic/portfolio/580x285-2.jpg" data-at2x="pic/portfolio/580x285-2@2x.jpg" alt="">
                        </div>
                        <div class="links">
                            <a href="pic/portfolio/580x285-2@2x.jpg" class="fancy"><i class="fa fa-expand"></i></a><br>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- ! gallery section-->
    <!-- latest news-->
    <section class="small-section cws_prlx_section bg-blue-40" id="cws_prlx_section_200435880345"><img src="http://kanvoy.com/pic/parallax-3.jpg" alt="" class="cws_prlx_layer" id="cws_prlx_layer_640085073480">
        <div class="container">
            <div class="row mb-50">
                <div class="col-md-8">
                    <h6 class="title-section-top font-4">ÚLTIMES NOTÍCIES</h6>
                    <h2 class="title-section alt-2"><span>EL NOSTRE</span> Blog</h2>
                    <div class="cws_divider mb-25 mt-5">
                        <br>
                    </div>
                    <p class="color-white">
                        Últimes notícies sobre nosaltres o quelcom que considerem interessant de donar a conèixer:
                    </p>
                </div>
                <div class="col-md-4">
                    <i class="flaticon-suntour-calendar title-icon alt"></i><br>
                </div>
            </div>
            <div class="carousel-container">
                <div class="row">
                    <div class="owl-two-pag pagiation-carousel mb-20">
                                    <?php $this->
                                    db->limit('3');
                                    $this->db->order_by('id', 'DESC');
                                    $blog = $this->db->get_where('blog');
                                    foreach ($blog->result() as $b):
                                        ?> 
                            <!-- Blog item-->
                            <div class="blog-item clearfix">
                                <!-- Blog Image-->
                                <div class="blog-media">
                                    <div class="pic">
                                        <img style="width: 270px; height: 270px;" src="<?= base_url('img/blog/' . $b->foto) ?>" data-at2x="<?= base_url('img/blog/' . $b->
                                            foto)
                                        ?>" alt="" data-mce-style="width: 270px; height: 270px;">
                                    </div>
                                </div>
                                <!-- blog body-->
                                <div class="blog-item-body clearfix">
                                    <!-- title-->
                                    <h6 class="blog-title"><?= $b->
                                    titulo
                                    ?></h6>
                                    <div class="blog-item-data">
    <?= date("d-m-Y", strtotime($b->
                    fecha))
    ?>
                                    </div>
                                    <a href="blog-single.html">
                                        <!-- Text Intro-->
                                        <!-- Text Intro-->
                                    </a>
                                    <p>
    <?= substr(strip_tags($b->
                    texto), 0, 90) . '...'
    ?>
                                    </p>
                                    <a href="<?= site_url('blog/' . toURL($b->id . '-' . $b->titulo)) ?>" class="blog-button">Leer más</a><br>
                                </div>
                            </div>
                            <!-- ! Blog item-->
<?php endforeach ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- ! latest news-->
    <!-- call out section-->
<?php $this->
load->view('includes/template/subscribe');
?>
</div>
