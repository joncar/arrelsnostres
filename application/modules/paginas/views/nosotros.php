<header><?php $this->
load->view('includes/template/header'); ?> 
<!-- breadcrumbs start-->
<section style="background-image: url(http://www.arrelsnostres.cat/new/pic/breadcrumbs/bg-1.jpg); background-repeat: no-repeat; background-position: center;" class="breadcrumbs" data-mce-style="background-image: url(http://www.arrelsnostres.cat/new/pic/breadcrumbs/bg-1.jpg); background-repeat: no-repeat; background-position: center;">
<div class="container">
	<div class="text-left breadcrumbs-item">
		<a href="#">Inici</a><i>/</i><a href="#" class="last"><span>Qui som?</span></a>
		<h2><span>QUI</span> SOm?</h2>
	</div>
</div>
</section>
<!-- ! breadcrumbs end-->
</header>
<div class="content-body">
	<!-- page section about-->
	<section class="small-section cws_prlx_section bg-white-80 pb-0"><img src="http://www.arrelsnostres.cat/new/pic/parallax-4.jpg" alt="" class="cws_prlx_layer">
	<div class="container">
		<div class="row">
			<div class="col-md-6 mb-md-60">
				<!-- section title-->
				<h2 class="title-section-top alt gray">QUÉ ÈS</h2>
				<h2 class="title-section alt gray mb-20 font-bold"><span>ARRELS</span> NOSTRES</h2>
				<!-- ! section title-->
				<p class="mb-30 " style=" color: #3d3d3d">
					Vestibulum tincidunt venenatis scelerisque. Proin quis enim lacinia, vehicula massa et, mollis urna. Proin nibh mauris, blandit vitae convallis at, tincidunt vel tellus. Praesent posuere nec lectus non cursus. Sed commodo odio et ipsum sagittis tincidunt.
				</p>
				<div class="cws_divider short mb-30">
					<br>
				</div>
				<h3 class="font-medium font-5">Productes amb Tradició<br>
				</h3>
			</div>
			<div class="col-md-6 flex-item-end">
				<img src="http://www.arrelsnostres.cat/new/pic/promo-2.png" alt="" class="mt-minus-100">
			</div>
		</div>
	</div>
	</section>
	<!-- ! page section about-->
	<!-- section parallax counter-->
	<section class="small-section">
	<div class="container">
		<div class="row">
			<!-- counter blocks-->
			<div class="col-md-6">
				<div class="row">
					<div class="col-xs-6 mt-20 mb-80">
						<div class="counter-block">
							<i class="counter-icon flaticoncustom-draw"></i>
							<div class="counter-name-wrap">
								<div data-count="345" class="counter">
									0
								</div>
								<div class="counter-name">
									Empreses<br>
								</div>
							</div>
						</div>
					</div>
					<div class="col-xs-6 mt-20 mb-80">
						<div class="counter-block">
							<i class="counter-icon flaticoncustom-hands"></i>
							<div class="counter-name-wrap">
								<div data-count="438" class="counter">
									0
								</div>
								<div class="counter-name">
									Productes<br>
								</div>
							</div>
						</div>
					</div>
					<div class="col-xs-6 mb-80">
						<div class="counter-block">
							<i class="counter-icon flaticoncustom-social"></i>
							<div class="counter-name-wrap">
								<div data-count="526" class="counter">
									0
								</div>
								<div class="counter-name">
									Sectors<br>
								</div>
							</div>
						</div>
					</div>
					<div class="col-xs-6 mb-80">
						<div class="counter-block">
							<i class="counter-icon flaticoncustom-commerce"></i>
							<div class="counter-name-wrap">
								<div data-count="169" class="counter">
									0
								</div>
								<div class="counter-name">
									Repercusió<br>
								</div>
							</div>
						</div>
					</div>
					<div class="col-xs-6">
						<div class="counter-block">
							<i class="counter-icon flaticoncustom-global"></i>
							<div class="counter-name-wrap">
								<div data-count="293" class="counter">
									0
								</div>
								<div class="counter-name">
									Clients<br>
								</div>
							</div>
						</div>
					</div>
					<div class="col-xs-6">
						<div class="counter-block">
							<i class="counter-icon flaticoncustom-business"></i>
							<div class="counter-name-wrap">
								<div data-count="675" class="counter">
									0
								</div>
								<div class="counter-name">
									Facturació<br>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- ! counter blocks-->
			<!-- tabs-->
			<div class="col-md-6 mt-md-40">
				<div class="tabs">
					<div class="block-tabs-btn clearfix">
						<div data-tabs-id="tabs1" class="tabs-btn active">
							FILOSOFIA
						</div>
						<div data-tabs-id="tabs2" class="tabs-btn">
							misió
						</div>
						<div data-tabs-id="tabs3" class="tabs-btn">
							VISIó
						</div>
					</div>
					<!-- tabs keeper-->
					<div class="tabs-keeper">
						<!-- tabs container-->
						<div data-tabs-id="cont-tabs1" class="container-tabs active">
							<h6 class="trans-uppercase">ELS NOSTRE equip<br>
							</h6>
							<p>
								Tenemos un equipo multidisciplinar, y todos y cada uno de sus componentes son pieza fundamental para que tu viaje, se desarrolle a la perfección. Nuestros asistentes/coordinadores podrán visitar vuestro instituto si así lo deseáis, explicándoos toda la EXPERIENCIA, y cómo poder conseguir contratarla al mejor precio y con toda la garantía de Kanvoy.&nbsp;
							</p>
						</div>
						<!-- /tabs container-->
						<!-- tabs container-->
						<div data-tabs-id="cont-tabs2" class="container-tabs">
							<h6 class="trans-uppercase">Bohemians</h6>
							<p>
								Duis egestas accumsan ipsum, at volutpat elit imperdiet in. Curabitur lacinia, massa quis elementum bibendum, tellus neque porttitor erat, a ornare enim arcu nec mauris. Morbi ac tristique felis. Praesent cursus placerat risus. Duis ut magna quis sem varius consequat.
							</p>
							<ul class="style-3">
								<li>Nam molestie dolor id auctor sodales;</li>
								<li>In sagittis dolor vel turpis aliquet pharetra;</li>
								<li>Quisque non turpis in dui congue dapibus;</li>
								<li>Vivamus varius nisl quis dictum maximus;</li>
								<li>Vestibulum scelerisque ligula quis est faucibus tincidunt.</li>
							</ul>
						</div>
						<!-- /tabs container-->
						<!-- tabs container-->
						<div data-tabs-id="cont-tabs3" class="container-tabs">
							<h6 class="trans-uppercase">Bohemians</h6>
							<p>
								Duis egestas accumsan ipsum, at volutpat elit imperdiet in. Curabitur lacinia, massa quis elementum bibendum, tellus neque porttitor erat, a ornare enim arcu nec mauris. Morbi ac tristique felis. Praesent cursus placerat risus. Duis ut magna quis sem varius consequat.
							</p>
							<ul class="style-3">
								<li>Nam molestie dolor id auctor sodales;</li>
								<li>In sagittis dolor vel turpis aliquet pharetra;</li>
								<li>Quisque non turpis in dui congue dapibus;</li>
								<li>Vivamus varius nisl quis dictum maximus;</li>
								<li>Vestibulum scelerisque ligula quis est faucibus tincidunt.</li>
							</ul>
						</div>
						<!-- /tabs container-->
					</div>
					<!-- /tabs keeper-->
				</div>
			</div>
			<!-- /tabs-->
		</div>
	</div>
	</section>
	<!-- ! section parallax counter-->
	<!-- ! testimonials section-->
    <section class="small-section cws_prlx_section bg-blue-40">
        <img src="http://www.arrelsnostres.cat/new/pic/parallax-2.jpg" alt class="cws_prlx_layer">
        <div class="container">
          <div class="row">
            <div class="col-md-8">
              <h6 class="title-section-top font-4">GENT FELIÇ</h6>
              <h2 class="title-section alt-2"><span>ELS CLIENTS</span> OPINEN</h2>
              <div class="cws_divider mb-25 mt-5"></div>
            </div>
          </div>
          <div class="row">
            <!-- testimonial carousel-->
            <div class="owl-three-item">
              <!-- testimonial item-->
              <div class="testimonial-item">
                <div class="testimonial-top"><a href="#">
                    <div class="pic"><img src="http://kanvoy.com/pic/testimonial/top-bg/1.jpg" data-at2x="http://kanvoy.com/pic/testimonial/top-bg/1@2x.jpg" alt></div></a>
                  <div class="author"> <img src="http://kanvoy.com/pic/testimonial/author/1.jpg" data-at2x="http://kanvoy.com/pic/testimonial/author/1@2x.jpg" alt></div>
                </div>
                <!-- testimonial content-->
                <div class="testimonial-body">
                  <h5 class="title"><span>Jordi</span> Magaña</h5>
                  <div class="stars stars-5"></div>
                  <p class="align-center">Suspe blandit orci quis lorem eleifend maximus. Quisque nec.</p>
                  <a href="http://kanvoy.com/p/nosotros.html#testimonios" class="testimonial-button">Tarragona</a>
                </div>
              </div>
              <!-- testimonial item-->
              <div class="testimonial-item">
                <div class="testimonial-top"><a href="#">
                    <div class="pic">
                        <img src="http://www.arrelsnostres.cat/new/pic/testimonial/top-bg/2.jpg" data-at2x="http://www.arrelsnostres.cat/new/pic/testimonial/top-bg/2@2x.jpg" alt>
                    </div></a>
                  <div class="author"> 
                      <img src="http://www.arrelsnostres.cat/new/pic/testimonial/author/2.jpg" data-at2x="http://www.arrelsnostres.cat/new/pic/testimonial/author/2@2x.jpg" alt>
                  </div>
                </div>
                <!-- testimonial content-->
                <div class="testimonial-body">
                  <h5 class="title"><span>Eliana</span> Tusell</h5>
                  <div class="stars stars-5"></div>
                  <p class="align-center">Nulla elit justo, dapibus ut lacus ac, ornare elementum neque.</p>
                  <a href="http://kanvoy.com/p/nosotros.html#testimonios" class="testimonial-button">Lleida</a>
                </div>
              </div>
              <!-- testimonial item-->
              <div class="testimonial-item">
                <div class="testimonial-top">
                    <a href="#">
                    <div class="pic">
                        <img src="http://www.arrelsnostres.cat/new/pic/testimonial/top-bg/3.jpg" data-at2x="http://www.arrelsnostres.cat/new/pic/testimonial/top-bg/3@2x.jpg" alt>
                    </div>
                    </a>
                  <div class="author"> 
                      <img src="http://www.arrelsnostres.cat/new/pic/testimonial/author/3.jpg" data-at2x="http://www.arrelsnostres.cat/new/pic/testimonial/author/3@2x.jpg" alt>
                  </div>
                </div>
                <!-- testimonial content-->
                <div class="testimonial-body">
                  <h5 class="title"><span>Miriam</span> Salazar</h5>
                  <div class="stars stars-5"></div>
                  <p class="align-center">Maece facilisis sit amet mauris eget aliquam. Integer vitae.</p>
                  <a href="http://kanvoy.com/p/nosotros.html#testimonios" class="testimonial-button">Valls</a>
                </div>
              </div>
              <!-- testimonial item-->
              <div class="testimonial-item">
                <div class="testimonial-top">
                    <a href="#">
                    <div class="pic">
                        <img src="http://www.arrelsnostres.cat/new/pic/testimonial/top-bg/1.jpg" data-at2x="http://www.arrelsnostres.cat/new/pic/testimonial/top-bg/1@2x.jpg" alt>
                    </div>
                    </a>
                    <div class="author"> 
                        <img src="http://www.arrelsnostres.cat/new/pic/testimonial/author/1.jpg" data-at2x="http://www.arrelsnostres.cat/new/pic/testimonial/author/1@2x.jpg" alt>
                    </div>
                </div>
                <!-- testimonial content-->
                <div class="testimonial-body">
                  <h5 class="title"><span>Jordi</span> Magaña</h5>
                  <div class="stars stars-5"></div>
                  <p class="align-center">Suspe blandit orci quis lorem eleifend maximus. Quisque nec.</p>
                  <a href="http://kanvoy.com/p/nosotros.html#testimonios" class="testimonial-button">Leer más</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
	<!-- ! team section -->
	<!-- call out section-->
	<section class="page-section cws_prlx_section bg-white-80 pb-60 pt-60"><img src="http://www.arrelsnostres.cat/new/pic/parallax-3.jpg" alt="" class="cws_prlx_layer">
	<div class="container">
		<div class="call-out-box">
			<div class="call-out-wrap alt">
				<h2 class="title-section alt-2 gray">TENS ALGUNA PREGUNTA?</h2>
				<a href="http://kanvoy.com/p/contacto" class="cws-button border-left large alt mb-20">CONTACTAR</a>
			</div>
		</div>
	</div>
	</section>
	<!-- ! call out section-->
</div>