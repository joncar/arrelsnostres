    <header>
        <?php $this->load->view('includes/template/header'); ?>
        <!-- breadcrumbs start-->
          <section style="background-image:url('http://kanvoy.com/pic/breadcrumbs/bg-1.jpg'); background-repeat: no-repeat; background-position: center;" class="breadcrumbs">
        <div class="container">
          <div class="text-left breadcrumbs-item">
              <a href="<?= site_url() ?>">Inicio</a><i>/</i>
              <a href="<?= site_url('blog') ?>">Blog</a><i>/</i>
              <a href="<?= site_url($detail->link) ?>" class="last"><?= $detail->titulo ?></a>
              <h2><?= $detail->titulo ?></h2>
          </div>
        </div>
      </section>
          <!-- ! breadcrumbs end-->
    </header>
    <div class="content-body">
      <div class="container page">
        <div class="row">
          <div class="col-md-8 mb-md-140">
            <!-- Blog Post image-->
            <div class="blog-item alt pb-20">
              <!-- Blog Image-->
              <div class="pic"><img src="<?= $detail->foto ?>" data-at2x="<?= $detail->foto ?>" alt></div>
              <!-- title, author...-->
              <div class="blog-item-data clearfix">
                <h3 class="blog-title"><?= $detail->titulo ?></h3>
                <p class="post-info">
                    <i class="flaticon-people"></i>
                    <span class="posr-author"><?= $detail->user ?>
                    en <?= date("D, d-m-Y",strtotime($detail->fecha)) ?>
                </p>
              </div>
              <!-- Text Intro-->
              <?= $detail->texto ?>
            </div>
            <!-- Blog Testimonials-->
            <div class="blog-item alt">
              <h2 class="title-section alt-3 font-bold mt-0 mb-10">Comentarios</h2>
              <div class="cws_divider"></div>
              <!-- comment list section-->
              <div class="comments mt-40">                
                <?php foreach($comentarios->result() as $c): ?>  
                  <div class="comment-body">
                    <div class="avatar">
                        <img src="http://kanvoy.com/pic/blog/90x90/1.jpg" data-at2x="http://kanvoy.com/pic/blog/90x90/1@2x.jpg" alt="">
                    </div>
                    <div class="comment-info">
                      <div class="comment-meta">
                        <div class="title">
                          <h5><?= $c->autor ?></h5>
                        </div>
                        <div class="comment-date">
                            <span><?= dias(date("D",strtotime($c->fecha))).', '.date('d-m-Y',strtotime($c->fecha)) ?></span>
                        </div>
                      </div>
                      <div class="comment-content">
                          <?= strip_tags($c->texto) ?>
                      </div>
                    </div>
                  </div>
                <?php endforeach ?>  
                <?php if($comentarios->num_rows()==0): ?>
                  <div class="comment-body">                    
                    Se el primero en comentar sobre este artículo
                  </div>
                <?php endif ?>
              </div>
              <!-- ! comment list section-->
            </div>
            <!-- Leave a comment-->
            <h2 class="title-section mt-50 mb-20"><span>Envia un comentario</span></h2>
            <div class="add-comment pattern bg-gray-3 relative">
              <div class="widget-contact-form pb-0">
                <!-- contact-form-->
                <div class="email_server_responce"></div>
                <form action="<?= base_url('blog/frontend/comentarios') ?>" method="post" class="contact-form form alt clearfix">
                  <?php if(!empty($_SESSION['mensaje'])){
                        echo $_SESSION['mensaje'];
                        unset($_SESSION['mensaje']);
                  }?>
                    <div class="row">
                    <div class="col-md-6">
                      <div class="input-container">
                        <input type="text" name="autor" value="" size="40" placeholder="Nombre" aria-invalid="false" aria-required="true" class="form-row form-row-first">
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="input-container">
                        <input type="text" name="email" value="" size="40" placeholder="Email" aria-required="true" class="form-row form-row-last">
                      </div>
                    </div>
                  </div>
                  <div class="input-container">
                    <textarea name="texto" cols="40" rows="4" placeholder="Comentario" aria-invalid="false" aria-required="true"></textarea>
                  </div>
                  <input type="hidden" name="blog_id" value="<?= $detail->id ?>">
                  <input type="submit" value="Enviar ahora" class="cws-button alt">
                </form>
                <!-- /contact-form-->
              </div>
            </div>
          </div>
          <div class="col-md-4 sidebar">
            <aside class="sb-right pb-50-imp">              
              <!-- widget category-->
              <div class="cws-widget">
                <div class="widget-categories">
                  <h2 class="widget-title">Categories</h2>
                  <ul>
                    <?php foreach($categorias->result() as $c): ?>
                        <li class="cat-item cat-item-1">
                            <a href="<?= site_url('blog') ?>?categorias_id=<?= $c->id ?>"><?= $c->blog_categorias_nombre ?></a>(<?= $c->cantidad ?>)
                        </li>
                    <?php endforeach ?>
                  </ul>
                </div>
              </div>
              <!-- ! widget category-->
              <!-- widget post-->
              <div class="cws-widget">
                <div class="widget-post">
                  <h2 class="widget-title alt">Entradas Relacionadas</h2>
                  
                  <?php foreach($relacionados->result() as $r): ?>
                    <!-- item recent post-->
                    <div class="item-recent clearfix">
                      <div class="widget-post-media">
                          <img src="<?= $r->foto ?>" data-at2x="<?= $r->foto ?>" alt>
                      </div>
                      <h3 class="title">
                          <a href="<?= $r->link ?>"><?= $r->titulo ?></a>
                      </h3>
                      <div class="date-recent"><?= date("D, d-m-Y",strtotime($r->fecha)) ?></div>
                    </div>
                    <!-- ! item recent post-->
                  <?php endforeach ?>
                  <?php if($relacionados->num_rows()==0): ?>
                    <div class="item-recent clearfix">
                      Sin artículos relacionados
                    </div>
                  <?php endif ?>
                </div>
              </div>
              <!-- ! widget post-->
              <!-- widget tags-->
              <div class="cws-widget">
                <div class="widget-tags">
                  <h2 class="widget-title">Etiquetas</h2>
                  <!-- item tags-->
                  <div class="widget-tags-wrap">
                      <?php if(!empty($detail->tags)): ?>
                            <?php foreach(explode(',',$detail->tags) as $t): ?>                                
                                <a href="<?= base_url('blog?direccion='.str_replace('#','',$t)) ?>" rel="tag" class="tag"><?= $t ?></a>                                
                            <?php endforeach ?>            
                      <?php endif ?>                      
                  </div>
                </div>
              </div>
              <!-- ! widget tags-->
              <!-- widget comments-->
            </aside>
          </div>
        </div>
      </div>
    </div>