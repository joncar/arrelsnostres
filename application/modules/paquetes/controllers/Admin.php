<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Admin extends Panel{
        function __construct() {
            parent::__construct();
        }
        
        function paquetes($action = '',$id = ''){
            $crud = $this->crud_function('','');
            $crud->set_field_upload('portada','img/paquetes');
            $crud->set_field_upload('portada_main','img/paquetes');
            $crud->set_field_upload('banner','img/paquetes');
            $crud->display_as('portada','Portada (720x240)');
            $crud->display_as('portada_main','Portada (480x350)');
            $crud->display_as('banner','Banner (1920x1280)');
            $crud->field_type('mapa','map',array());
            $crud->field_type('actividades','tags');
            $crud->add_action('<i class="fa fa-envelope"></i> Comentarios','',base_url('paquetes/admin/paquetes').'/');
            $crud->add_action('<i class="fa fa-image"></i> Galeria de fotos','',base_url('paquetes/admin/paquetes_fotos').'/');
            $crud->add_action('<i class="fa fa-image"></i> Reservas','',base_url('paquetes/admin/reservas').'/');
            $crud = $crud->render();
            $this->loadView($crud);
        }   
                
        
        function paquetes_comentarios($action = '',$id = ''){
            $crud = $this->crud_function('','');
            $crud->where('paquetes_id',$action);
            $crud->field_type('paquetes_id','hidden',$action);            
            $crud = $crud->render();
            $this->loadView($crud);
        }
        
        function reservas($action = '',$id = ''){
            $crud = $this->crud_function('','');
            $crud->where('paquetes_id',$action);
            $crud->field_type('paquetes_id','hidden',$action);            
            $crud = $crud->render();
            $this->loadView($crud);
        }
        
        function paquetes_fotos(){
            $this->load->library('image_crud');
            $crud = new image_CRUD();
            $crud->set_table('paquetes_fotos')
                 ->set_url_field('foto')
                 ->set_relation_field('paquetes_id')
                 ->set_image_path('img/paquetes')
                 ->module = 'paquetes';
            $crud = $crud->render();
            $crud->output.= '<p><b>Nota: </b> El tamaño recomendado para las fotos es de 1170x480</p>';
            $crud->title = 'Galería Fotográfica de paquetes';
            $this->loadView($crud);
        }
    }
?>
