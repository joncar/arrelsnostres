<div class="col-md-6">
    <div class="recom-item border">
        <div class="recom-media">
            <a href="<?= $d->link ?>">
                <div class="pic">
                    <img src="<?= $d->portada ?>" data-at2x="<?= $d->portada ?>" alt="" style="width: 720px;">
                </div>
            </a>
            <div class="location">
                <i class="flaticon-suntour-map"></i> <?= $d->ciudad ?>
            </div>
        </div>
        <!-- Recomended Content-->
        <div class="recom-item-body">
            <a href="<?= $d->link ?>">
                <h6 class="blog-title"><?= $d->nombre ?></h6>
            </a>

            <div class="recom-price">
                Desde <span class="font-4">€<?= $d->precio_desde ?></span>
            </div>
            <p class="mb-30"><?= $d->descripcion_corta ?></p>
                <a href="<?= $d->link ?>" class="recom-button">Ver Destinos</a>
            <div class="action font-2">20%</div>
        </div>
        <!-- Recomended Image-->
    </div>
</div>