<!DOCTYPE html>
<html lang="en-us" class="no-js">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">          
        <title><?= empty($title)?'Arrelsnostres':$title ?></title>         
        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <link rel="stylesheet" href="<?= base_url() ?>css/template/reset.css">
        <link rel="stylesheet" href="<?= base_url() ?>css/template/bootstrap.css">
        <link rel="stylesheet" href="<?= base_url() ?>css/template/font-awesome.css">
        <link rel="stylesheet" href="<?= base_url() ?>css/template/owl.carousel.css">
        <link rel="stylesheet" href="<?= base_url() ?>css/template/jquery.fancybox.css">
        <link rel="stylesheet" href="<?= base_url() ?>css/fonts/fi/flaticon.css">
        <link rel="stylesheet" href="<?= base_url() ?>css/fonts/fi2/font/flaticon.css">
        <link rel="stylesheet" href="<?= base_url() ?>css/template/flexslider.css">
        <link rel="stylesheet" href="<?= base_url() ?>css/template/main.css">
        <link rel="stylesheet" href="<?= base_url() ?>css/template/indent.css">
        <link rel="stylesheet" href="<?= base_url() ?>rs-plugin/css/settings.css">
        <link rel="stylesheet" href="<?= base_url() ?>rs-plugin/css/layers.css">
        <link rel="stylesheet" href="<?= base_url() ?>rs-plugin/css/navigation.css">
        
        <script src="http://code.jquery.com/jquery-1.10.0.js"></script>		
    </head>
    <body>              
        <?= $this->load->view($view); ?>      
        <?= $this->load->view('includes/template/footer'); ?>      
        <?= $this->load->view('includes/template/modal'); ?>      
        <?php if(empty($scripts)){$this->load->view('includes/template/scripts');} ?>      
    </body>   
</html>