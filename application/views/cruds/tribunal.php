<?php $this->load->view('predesign/chosen'); ?>
<form action="" method="post">
    <div>
        <div id="detalles_field_box" class="form-group">
        <label id="plan_estudio_id_display_as_box" for="field-detalles_id">
                Asignación de Tribunal
        </label>
            <div>
                <div class="row">
                    <div class="col-xs-10"><b>Docente</b></div>
                </div>
                <?php if(!empty($detalles)): ?>
                    <?php foreach($detalles->result() as $d): ?>
                        <div class="row programacionMateriasPlan">
                            <div class="col-xs-10"><?= form_dropdown_from_query('docente[]',$this->planAcademico_model->getDocentes(),'id','nombre apellido_paterno',$d->docentes_id,'',TRUE,'form-control materiaField','Seleccione') ?></div>
                            <div class="col-xs-2"><a href="#" class='add'><i class="fa fa-plus-circle"></i></a> <a href="#" class='remove' style="color:red"><i class="fa fa-remove"></i></a></div>
                        </div>
                    <?php endforeach ?>
                <?php endif ?>
                <div class="row programacionMateriasPlan">
                    <div class="col-xs-10"><?= form_dropdown_from_query('docente[]',$this->planAcademico_model->getDocentes(),'id','nombre apellido_paterno',0,'',TRUE,'form-control materiaField','Seleccione') ?></div>
                    <div class="col-xs-2"><a href="#" class='add'><i class="fa fa-plus-circle"></i></a> <a href="#" class='remove' style="color:red"><i class="fa fa-remove"></i></a></div>
                </div>
            </div>
        </div>
        <?php if($success): ?>
            <div class="alert alert-success">Se ha guardado los datos con éxito</div>
        <?php endif ?>
        <div class="btn-group">
            <button type="submit" class="btn btn-success">Asignar</button>
            <a href="<?= base_url('procesosacademicos/horario_examenes') ?>" class="btn btn-default">Volver Atras</a>
        </div>
    </div>
</form>
<script src='<?= base_url('js/jquery.mask.js'); ?>'></script>
<script>
    function add(obj){
        obj = $(obj).parents('.programacionMateriasPlan');
        var element = obj.clone();
        obj.after(element);
        var index = obj.index();
        var newRow = $(".programacionMateriasPlan")[index];
        newRow = $(newRow);
        newRow.find('input[type="date"], input[type="time"]').val('');
        newRow.find('select').attr('id',index+'_chzn');
        $("select").removeClass('chzn-done');
        $(".chzn-container").remove();
	$(".chosen-select,.chosen-multiple-select").chosen({allow_single_deselect:true});
    }
    function remove(obj){
        $(obj).parents('.programacionMateriasPlan').remove();
    }
    $(document).on('click','.remove',function(e){
        e.preventDefault();
        remove($(this));
    });
    $(document).on('click','.add',function(e){
        e.preventDefault();
        add($(this));
    });
</script>