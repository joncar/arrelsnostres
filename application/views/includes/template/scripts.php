<!-- ! news popup-->
<script src="https://www.youtube.com/player_api"></script>
<script src="http://code.jquery.com/jquery-1.10.0.js"></script>	
<script type="text/javascript" src="<?= base_url() ?>js/template/jquery-ui.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>js/template/bootstrap.js"></script>
<script type="text/javascript" src="<?= base_url() ?>js/frame.js"></script>


<script type="text/javascript" src="<?= base_url() ?>js/template/owl.carousel.js"></script>
<script type="text/javascript" src="<?= base_url() ?>js/template/jquery.sticky.js"></script>
<script type="text/javascript" src="<?= base_url() ?>js/template/TweenMax.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>js/template/cws_parallax.js"></script>
<script type="text/javascript" src="<?= base_url() ?>js/template/jquery.fancybox.pack.js"></script>
<script type="text/javascript" src="<?= base_url() ?>js/template/jquery.fancybox-media.js"></script>
<script type="text/javascript" src="<?= base_url() ?>js/template/isotope.pkgd.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>js/template/imagesloaded.pkgd.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>js/template/masonry.pkgd.min.js"></script>        
<script type="text/javascript" src="<?= base_url() ?>js/template/jquery.validate.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>js/template/jquery.form.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>js/template/script.js"></script>
<script type="text/javascript" src="<?= base_url() ?>js/template/bg-video/cws_self_vimeo_bg.js"></script>
<script type="text/javascript" src="<?= base_url() ?>js/template/bg-video/jquery.vimeo.api.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>js/template/bg-video/cws_YT_bg.js"></script>
<script type="text/javascript" src="<?= base_url() ?>js/template/jquery.tweet.js"></script>
<script type="text/javascript" src="<?= base_url() ?>js/template/jquery.scrollTo.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>js/template/jquery.flexslider.js"></script>
<script type="text/javascript" src="<?= base_url() ?>js/template/retina.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>rs-plugin/js/jquery.themepunch.tools.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>rs-plugin/js/extensions/revolution.extension.slideanims.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>rs-plugin/js/extensions/revolution.extension.layeranimation.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>rs-plugin/js/extensions/revolution.extension.navigation.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>rs-plugin/js/extensions/revolution.extension.parallax.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>rs-plugin/js/extensions/revolution.extension.video.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>rs-plugin/js/extensions/revolution.extension.actions.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>rs-plugin/js/extensions/revolution.extension.kenburn.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>rs-plugin/js/extensions/revolution.extension.migration.min.js"></script>