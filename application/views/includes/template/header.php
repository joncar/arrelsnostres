    <div class="site-top-panel">
        <div class="container p-relative">
            <div class="row">
                <div class="visible-lg col-xs-12 col-md-4">
                    <div class="top-left-wrap font-3">
                        <div class="mail-top"><a href="mailto:info@arrelsnostres.cat"> <i class="flaticon-suntour-email"></i>info@arrelsnostres.cat</a></div><span>/</span>
                        <div class="tel-top"><a href="93 805 37 38"> <i class="flaticon-suntour-phone"></i>93 805 37 38</a></div>
                    </div>
                    <!-- ! lang select wrapper-->
                </div>
                <div class="col-xs-12 col-md-8 text-right">
                    <div class="top-right-wrap">
                        <div><a href="http://www.canmabres.com">CARN CAN MABRES</a></div>/ 
                        <div><a href="http://www.fermibohigas.com">CAVES BOHIGES</a></div> / 
                        <div><a href="http://www.fruitssecstorra.com/ca/">FRUITS SECS SANT JORDI</a></div> / 
                        <div><a href="http://www.fruitssp.com">SALSES FRUITS S&P</a></div>   
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
    <nav class="main-nav js-stick">
        <div class="full-wrapper relative clearfix container">
            <div class="nav-logo-wrap local-scroll">
                <a href="<?= site_url() ?>" class="logo">
                    <img src="<?= base_url() ?>img/logo.png">
                </a>
            </div>
            <div class="inner-nav desktop-nav">
                <ul class="clearlist">              
                    <li><a href="<?= site_url() ?>" class="active">Inici</a></li>              
                    <li class="slash">/</li>              
                    <li><a href="<?= site_url('p/nosotros') ?>">Qui som?</a></li>
                    <li class="slash">/</li>
                    <li><a href="<?= site_url('p/servicios') ?>">Serveis</a></li>
                    <li class="slash">/</li>
                    <li>
                        <a href="<?= site_url('empresas') ?>" class="mn-has-sub">
                            EMPRESES <i class="fa fa-angle-down button_open"></i>
                        </a>
                        <ul class="mn-sub">
                            <?php $this->db->limit(4); $this->db->order_by('id','DESC'); ?>
                            <?php foreach ($this->db->get('empresas')->result() as $g): ?>
                            <li><a href="<?= site_url('empresa/'.toURL($g->id.'-'.$g->nombre)) ?>"><?= $g->nombre ?></a></li>
                            <?php endforeach ?>
                        </ul>
                    </li>
                    <li class="slash">/</li>
                    <li><a href="<?= site_url('blog') ?>">Notícies</a></li>
                    
                    <li class="slash">/</li>
                    <li><a href="<?= site_url('p/contacto') ?>">Contacte</a></li>
                    <li class="search"><a href="#" class="mn-has-sub">Buscar</a>
                        <ul class="search-sub">
                            <li>
                                <div class="container">
                                    <div class="mn-wrap">
                                        <form method="post" class="form">
                                            <div class="search-wrap">
                                                <input type="text" placeholder="que vols buscar?" class="form-control search-field"><i class="flaticon-suntour-search search-icon"></i>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="close-button"><span>Buscar</span></div>
                                </div>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </nav>