<!-- footer-->
    <footer style="background-image: url(http://www.arrelsnostres.cat/new/pic/footer/footer-bg.jpg)" class="footer footer-fixed">
      <div class="container">
        <div class="row pb-100 pb-md-40">
          <!-- widget footer-->
          <div class="col-md-6 col-sm-12 mb-sm-30">
            <div class="logo-soc clearfix">
              <div class="footer-logo"><a href="index.html"><img src="http://www.arrelsnostres.cat/new/img/logo-white.png" data-at2x="http://www.arrelsnostres.cat/new/img/img/logo-white@2x.png" alt></a></div>
            </div>
            <p class="color-g2 mt-10">Vestibulum tincidunt venenatis scelerisque. Proin quis enim lacinia, vehicula massa et, mollis urna. Proin nibh mauris, blandit vitae convallis at, tincidunt vel tellus. Praesent posuere nec lectus non.</p>
            <!-- social-->
            <div class="social-link dark"><a href="#" class="cws-social fa fa-twitter"></a><a href="#" class="cws-social fa fa-facebook"></a><a href="#" class="cws-social fa fa-google-plus"></a><a href="#" class="cws-social fa fa-linkedin"></a></div>
            <!-- ! social-->
          </div>
          <!-- ! widget footer-->
          <!-- widget footer-->
          <div class="col-md-3 col-sm-6 mb-sm-30">
            <div class="widget-footer">
              <h4>Adreça</h4><br>
              <a href="#" rel="tag" class="tag color-g2">President Lluis Companys 28<br>IGUALDA / BARCELONA C.P.08700</a>
              <h4>Telèfon d'Atenció</h4><br>
              <a href="#" rel="tag" class="tag color-g2">93 805 37 38</a>
              
            </div>
          </div>
          <!-- end widget footer-->
          <!-- widget footer-->
          <div class="col-md-3 col-sm-6">
            <div class="widget-footer">
              <h4>Tags</h4>
              <div class="widget-tags-wrap"><a href="#" rel="tag" class="tag">Salses</a><a href="#" rel="tag" class="tag">Caves </a><a href="#" rel="tag" class="tag">Vins</a><a href="#" rel="tag" class="tag">Carn</a><a href="#" rel="tag" class="tag">Fruits secs</a><a href="#" rel="tag" class="tag">Island</a><a href="#" rel="tag" class="tag">Postres/a><a href="#" rel="tag" class="tag">Vinyes</a><a href="#" rel="tag" class="tag">Vedella</a></div>
            </div>
          </div>
          <!-- end widget footer-->
        </div>
      </div>
      <!-- copyright-->
      <div class="copyright"> 
        <div class="container">
          <div class="row">
            <div class="col-sm-6">
              <p>© Copyright 2017 <span>ARRELS NOSTRES</span> &nbsp;&nbsp;|&nbsp;&nbsp; Tots els drets reservats</p>
            </div>
            <div class="col-sm-6 text-right">
                <a href="<?= site_url() ?>" class="footer-nav">Inici</a>
                <a href="<?= site_url('p/nosotros') ?>">Qui som?</a>
                <a href="<?= site_url('p/servicios') ?>">Serveis</a>
                <a href="<?= site_url('grupos') ?>" class="mn-has-sub">Empreses</a>
              	<a href="<?= site_url('p/contacto') ?>" class="mn-has-sub">Contacte</a>
            </div>
          </div>
        </div>
      </div>
      <!-- end copyright-->
      <!-- scroll top-->
    </footer>
    <div id="scroll-top"><i class="fa fa-angle-up"></i></div>